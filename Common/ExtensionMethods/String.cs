using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessFramework.Client.Common.ExtensionMethods
{
    /// <summary>
    /// The responsibility of this class is to contain extension methods for strings
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-08-19<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public static class String
    {
        /// <summary>
        /// Returns null for empty strings, otherwise the value of the string. Values are trimmed before check.
        /// </summary>
        public static string NullEmpty(this string str)
        {
            if (str == null)
            {
                return null;
            }

            return string.IsNullOrEmpty(str.Trim()) ? null : str;
        }
    }
}
