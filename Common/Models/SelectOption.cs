﻿using System;
using System.Runtime.Serialization;

namespace ProcessFramework.Client.Common.Models
{
    /// <summary>
    /// The purpose of this class is to encapsulate an item in a drop down list.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    [Serializable]
    [DataContract]
    public sealed class SelectOption
    {
        /// <summary>
        /// Lookup Id
        /// </summary>
        [DataMember]
        public int Id { get; set; }

        /// <summary>
        /// Description/Label for the lookup.
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// First item in the list flagged as default is used as default option. Else uses select.
        /// </summary>
        [DataMember]
        public bool IsDefault { get; set; }
    }
}