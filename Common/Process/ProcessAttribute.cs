﻿using System;

using ProcessFramework.Client.Common.Constants;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to provide a class decoration to indicate that the type is a process.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2012-06-26<br/>
    /// </remarks>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ProcessAttribute : Attribute
    {
        /// <summary>
        /// Description / Name of the process.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Create a new ProcessAttribute instance with a description of the process.
        /// </summary>
        public ProcessAttribute(string description)
        {
            Description = description;
        }

        /// <summary>
        /// Gets or sets the required permissions flag.
        /// </summary>
        public Permission RequiredPermissions { get; set; }
    }
}
