﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The purpose of this attribute is to indicate that the decorated class is a a base class to constrain a state transition return type.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class ConstrainDynamicTransitionBaseAttribute : Attribute
    {
    }
}
