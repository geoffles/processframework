using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to ... <some diligent developer to fill this in.>
    /// It collaborates with ... <same diligent developer to fill this in>
    /// <remarks>
    /// Created By  : firstname lastname<br/>
    /// Date        : 2013-mm-dd<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public interface IProcessManagerStateProvider
    {
        /// <summary>
        /// Gets the instance of the process manager.
        /// </summary>
        ProcessManager GetProcessManager();

        /// <summary>
        /// Gets the instance of the dictionary of processes.
        /// </summary>
        IDictionary<Guid, IProcess> GetProcesses();
    }
}
