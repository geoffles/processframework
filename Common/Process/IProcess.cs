﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using ProcessFramework.Client.Common.Validation;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to provide the interface for processes as used by the process manager.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2012-06-26<br/>
    /// </remarks>
    /// </summary>
    public interface IProcess
    {
        EnvironmentConfiguration EnvironmentConfiguration { get; }

        /// <summary>
        /// The current model state of the process.
        /// </summary> 
        object Model { get; }

        /// <summary>
        /// The current process state of the process. All transitions are invoked on this member
        /// </summary>
        IProcessState CurrentState { get; }

        /// <summary>
        /// The unique identifier for this process.
        /// </summary>
        Guid TabId { get; }

        /// <summary>
        /// The list of validation errors for this process.
        /// </summary>
        IList<ProcessValidationError> ValidationErrors { get; }

        /// <summary>
        /// The last caller to invoke a transition on this process.
        /// </summary>
        ProcessEntrantInfo LastCaller { get; set; }

        /// <summary>
        /// The initialisation method of the process.
        /// </summary>
        void Initialise(Guid tabId);

        /// <summary>
        /// Method to invoke a transition on this process.
        /// </summary>
        /// <param name="transition">MethodInfo of the transition to invoke</param>
        /// <param name="args">(optional) argument for the transition (eg: search criteria, or new model state)</param>
        void InvokeTransition(MethodInfo transition, object args);
    }
}
