﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to provide the interface for process states as used by the process manager.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2012-06-26<br/>
    /// </remarks>
    /// </summary>
    public interface IProcessState
    {
        /// <summary>
        /// Delegate to access the process model state from the process state implementations.
        /// </summary>
        ProcessModelAccessor ModelAccessor { get; set; }

        /// <summary>
        /// Executed when entering this state
        /// </summary>
        void Enter();
    }
}
