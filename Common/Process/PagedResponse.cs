﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to encapsulate a paged server response to the web application.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2012-07-4<br/>
    /// </remarks>
    /// </summary>
    public class PagedResponse<T> where T:class
    {
        /// <summary>
        /// The total number of items for the query.
        /// </summary>
        public long Total { get; set; }

        /// <summary>
        /// The data for this page.
        /// </summary>
        public IList<T> Data { get; set; }

    }
}
