using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProcessFramework.Client.Common.Validation;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to provide the base functionality for processes.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-07-18<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    [Serializable]
    public abstract class ProcessBase<T> : IProcess
    {
        /// <summary>
        /// Gets or sets the environment configuration.
        /// </summary>
        public EnvironmentConfiguration EnvironmentConfiguration { get; set; }

        /// <summary>
        /// Creates an instance of the process base and initialises the validation errors list.
        /// </summary>
        protected ProcessBase()
        {
            ValidationErrors = new List<ProcessValidationError>();
            EnvironmentConfiguration = new EnvironmentConfiguration();
        }

        /// <summary>
        /// Boxed process model accessor for interface.
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        public object Model
        {
            get { return TypedModel; }
        }

        /// <summary>
        /// The process model.
        /// </summary>
        public virtual T TypedModel { get; protected set; }

        /// <summary>
        /// The unique identifier for this process.
        /// </summary>
        public Guid TabId { get; protected set; }

        /// <summary>
        /// The last caller to invoke a transition on this process.
        /// </summary>
        public ProcessEntrantInfo LastCaller { get; set; }

        private IProcessState _currentState;

        public virtual IProcessState CurrentState
        {
            get { return _currentState; }
            protected set
            {
                value.ModelAccessor = new ProcessModelAccessor(this);
                _currentState = value;
            }
        }

        /// <summary>
        /// Called when the process is being initialised. Implement with initialisation logic.
        /// </summary>
        protected abstract void OnInitialise();

        /// <summary>
        /// Invoked by the framework to initialise the process. Use OnInitialise to perform initialisation logic.
        /// </summary>
        public virtual void Initialise(Guid tabId)
        {
            TabId = tabId;
            OnInitialise();
        }


        /// <summary>
        /// Method to invoke a transition on this process.
        /// </summary>
        /// <param name="transition">MethodInfo of the transition to invoke</param>
        /// <param name="args">(optional) argument for the transition (eg: search criteria, or new model state)</param>
        public void InvokeTransition(System.Reflection.MethodInfo transition, object args)
        {
            IProcessState newState;
            if (!transition.GetParameters().Any())
            {
                newState = (IProcessState)transition.Invoke(CurrentState, null);
            }
            else
            {
                newState = (IProcessState)transition.Invoke(CurrentState, new[] { args });
            }

            CurrentState = newState;
            CurrentState.Enter();
        }

        public IList<ProcessValidationError> ValidationErrors { get; protected set; } 
    }
}
