﻿using System.Web;
using System.Web.Mvc;

namespace ProcessFramework.Client.Common.Process
{
    public static class RazorExtensions
    {
        private const string CHANGE_VIEW = "javascript:$ChangeView('','{1}',{2});";

        /// <summary>
        /// Gets the JavaScript API handler. Usage: &lt;a onclick="@Url.ApiAction(Controller.ActionLinks.DoSomething, "getModel")" /&gt;
        /// </summary>
        /// <param name="url">The page's UrlHelper instance.</param>
        /// <param name="actionUrl">The API action to invoke</param>
        /// <param name="modelFn">This is a JavaScript function to be invoked in order to get the view model state</param>
        public static IHtmlString ApiAction(this UrlHelper url, string actionUrl, string modelFn)
        {
            string handler = string.Format(CHANGE_VIEW, null, actionUrl, modelFn);
            return new HtmlString(handler);
        }

        /// <summary>
        /// Gets the JavaScript Next handler. Usage: &lt;a onclick="@Url.GetNext(Model, "getModel")" /&gt;
        /// </summary>
        /// <param name="url">The page's UrlHelper instance.</param>
        /// <param name="model">The process meta model.</param>
        /// <param name="modelFn">This is a JavaScript function to be invoked in order to get the view model state</param>
        public static IHtmlString GetNext(this UrlHelper url, ProcessMetaModel model, string modelFn)
        {
            const TransitionStereoType TRANSITION = TransitionStereoType.Next;
            string handler = string.Format(CHANGE_VIEW, model.ViewFor(TRANSITION, url), model.DataFor(TRANSITION, url), modelFn);
            return new HtmlString(handler);
        }

        /// <summary>
        /// Gets the JavaScript back handler. Usage: &lt;a onclick="@Url.GetBack(Model, "getModel")" /&gt;
        /// </summary>
        /// <param name="url">The page's UrlHelper instance.</param>
        /// <param name="model">The process meta model.</param>
        /// <param name="modelFn">This is a JavaScript function to be invoked in order to get the view model state</param>
        public static IHtmlString GetBack(this UrlHelper url, ProcessMetaModel model, string modelFn)
        {
            const TransitionStereoType TRANSITION = TransitionStereoType.Back;
            string handler = string.Format(CHANGE_VIEW, model.ViewFor(TRANSITION, url), model.DataFor(TRANSITION, url), modelFn);
            return new HtmlString(handler);
        }

        /// <summary>
        /// Gets the JavaScript back handler. Usage: &lt;a onclick="@Url.GetBack(Model, "getModel")" /&gt;
        /// </summary>
        /// <param name="url">The page's UrlHelper instance.</param>
        /// <param name="model">The process meta model.</param>
        /// <param name="modelFn">This is a JavaScript function to be invoked in order to get the view model state</param>
        public static IHtmlString GetExit(this UrlHelper url, ProcessMetaModel model, string modelFn)
        {
            // ReSharper disable Mvc.ActionNotResolved
            // ReSharper disable Mvc.ControllerNotResolved

            const TransitionStereoType TRANSITION = TransitionStereoType.Exit;
            string handler = string.Format(CHANGE_VIEW, url.Action("BlankView", "Home", new { Area = "" }), model.DataFor(TRANSITION, url), modelFn);
            return new HtmlString(handler);

            // ReSharper restore Mvc.ControllerNotResolved
            // ReSharper restore Mvc.ActionNotResolved
        }
    }
}
