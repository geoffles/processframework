﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to provide information necessary for routing to a controller action.
    /// <remarks>
    /// Intended correlate with routing parameters {area}/{action}/{controller}
    /// 
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2012-06-26<br/>
    /// </remarks>
    /// </summary>
    public class ResourceIdentifier
    {
        /// <summary>
        /// The name for the controller
        /// </summary>
        public string ControllerName { get; set; }

        /// <summary>
        /// The name of the action to be invoked
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// The name of the area for the controller
        /// </summary>
        public string AreaName { get; set; }
    }
}
