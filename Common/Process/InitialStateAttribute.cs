﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to provide a class decoration to indicate that the type is the initial state in it's process.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2012-06-26<br/>
    /// </remarks>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class|AttributeTargets.Interface)]
    public class InitialStateAttribute : Attribute
    {
        /// <summary>
        /// The data controller associated with initialising the process
        /// </summary>
        public Type ApiController { get; private set; }

        /// <summary>
        /// The name for ApiController
        /// </summary>
        public string ControllerName
        {
            get
            {
                return ApiController
                    .GetFields(BindingFlags.Static | BindingFlags.Public)
                    .Single(f => f.Name == "NameConst")
                    .GetRawConstantValue()
                    .ToString();
            }
        }

        /// <summary>
        /// The area for ApiController
        /// </summary>
        public string AreaName
        {
            get
            {
                return ApiController
                    .GetFields(BindingFlags.Static | BindingFlags.Public)
                    .Single(f => f.Name == "AreaConst")
                    .GetRawConstantValue()
                    .ToString();
            }
        }

        /// <summary>
        /// Create a new instance of the InitialStateAttribute against the data controller.
        /// </summary>
        public InitialStateAttribute(Type apiController)
        {
            ApiController = apiController;
        }

    }
}
