using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to contain information about the type and method last invoked which resulted in a transition on this process.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-09-09<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    [Serializable]
    public sealed class ProcessEntrantInfo
    {
        /// <summary>
        /// The type of the object which last invoked a transition
        /// </summary>
        public Type StateType { get; set; }

        /// <summary>
        /// The method on <c>Type</c> which last invoked a transition.
        /// </summary>
        public MethodInfo Transition { get; set; }
    }
}
