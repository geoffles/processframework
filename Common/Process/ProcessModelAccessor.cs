using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to ... <some diligent developer to fill this in.>
    /// It collaborates with ... <same diligent developer to fill this in>
    /// <remarks>
    /// Created By  : firstname lastname<br/>
    /// Date        : 2013-mm-dd<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    [Serializable]
    public class ProcessModelAccessor
    {
        public ProcessModelAccessor(IProcess process)
        {
            Process = process;
        }

        public IProcess Process { get; private set; }

        public object GetModel()
        {
            return Process.Model;
        }
    }
}
