﻿using System;
using System.Linq;
using System.Reflection;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to provide a method decoration to indicate that the method represents a state transition.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2012-06-26<br/>
    /// </remarks>
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = true)]
    public class TransitionAttribute : Attribute
    {
        /// <summary>
        /// Type of the data controller for this transition
        /// </summary>
        public Type ApiController { get; private set; }

        /// <summary>
        /// Action name for the transition
        /// </summary>
        public string ActionName { get; private set; }

        /// <summary>
        /// The stereo type for this transition
        /// </summary>
        public TransitionStereoType StereoType {get; private set; }

        /// <summary>
        /// The name of the controller. Sourced from ApiController.
        /// </summary>
        public string ControllerName
        {
            get
            {
                return ApiController
                    .GetFields(BindingFlags.Static | BindingFlags.Public)
                    .Single(f => f.Name == "NameConst")
                    .GetRawConstantValue()
                    .ToString();
            }
        }

        /// <summary>
        /// The name from the controller's area. Sourced from ApiController.
        /// </summary>
        public string AreaName
        {
            get
            {
                return ApiController
                    .GetFields(BindingFlags.Static | BindingFlags.Public)
                    .Single(f => f.Name == "AreaConst")
                    .GetRawConstantValue()
                    .ToString();
            }
        }

        /// <summary>
        /// Create a new instance from the transition attribute.
        /// </summary>
        /// <param name="apiController">The data controller</param>
        /// <param name="action">The action to web action name to invoke</param>
        /// <param name="stereoType">(optional) A stereotype for the transition</param>
        public TransitionAttribute(Type apiController, string action, TransitionStereoType stereoType = TransitionStereoType.None)
        {
            ApiController = apiController;
            ActionName = action;
            StereoType = stereoType;
        }
    }
}
