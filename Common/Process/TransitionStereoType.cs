﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to provide process transition stereo types.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2012-06-26<br/>
    /// </remarks>
    /// </summary>
    public enum TransitionStereoType
    {
        /// <summary> Default - no stereotype </summary>
        None,

        /// <summary> Next step in process </summary>
        Next,

        /// <summary> Previous step in process </summary>
        Back,

        /// <summary> Exit the process </summary>
        Exit,

        /// <summary> Go back to first step in process </summary>
        First,

        /// <summary> Go forward to last step in process </summary>
        Last,

        FileDownload
    }
}
