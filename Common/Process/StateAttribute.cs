using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to ... <some diligent developer to fill this in.>
    /// It collaborates with ... <same diligent developer to fill this in>
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-08-06<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public sealed class StateAttribute : Attribute
    {
        public string Label;
        public int Order;
    }
}
