﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcessFramework.Client.Common.Process
{
    [Serializable]
    public class WrapAroundProcessCompletedResult : ProcessCompletedResult
    {
        public WrapAroundProcessCompletedResult()
        {
            WrapAroundProcess = true;
        }
    }
}
