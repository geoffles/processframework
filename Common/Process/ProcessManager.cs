﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using ProcessFramework.Client.Common.Context;
using ProcessFramework.Client.Common.Process.Menu;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class serve as the interface between the View and Api controllers and the processes.
    /// 
    /// This class depends on the declaration of processes in loaded assemblies and will facilitate 
    /// requests for process interaction  based on the calling controller and method and the mapping.
    /// 
    /// Using requires that the manager be attached to the web method. A transition call can then be made
    /// to the manager with the appropriate model for the transition, and the new model state will be returned.
    /// eg: 
    ///     ProcessManager processManager = new ProcessManager() // Performed by framework
    ///     processManager.Attach();                             // Performed by Framework
    /// 
    ///     (ProcessModelType) processManager.Transition&lt;(ProcessModelType)&gt;(viewModel); 
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2012-06-26<br/>
    /// </remarks>
    /// </summary>
    [Serializable]
    public class ProcessManager
    {
        /// <summary>
        /// Gets the current process manager instance
        /// </summary>
        /// <value>
        /// The instance.
        /// </value>
        [System.Xml.Serialization.XmlIgnore] 
        public static ProcessManager Instance
        {
            get { return ObjectFactory.Resolve<IProcessManagerStateProvider>().GetProcessManager(); }
        }


        /// <summary>
        /// Dictionary of processes in the session.
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        private IDictionary<Guid, IProcess> Processes
        {
            get { return ObjectFactory.Resolve<IProcessManagerStateProvider>().GetProcesses(); }
        }

        /// <summary>
        /// Indicates whether Process transitions should be suppressed.
        /// </summary>
        /// <remarks>
        /// Transitions are suppress when reloading the page. This effectively replays the result from the last transition.
        /// </remarks>
        [System.Xml.Serialization.XmlIgnore]
        public bool SupressTransitions { get; protected set; }

        /// <summary>
        /// The process associated with the current webaction. This is set by Attach();
        /// </summary>
        public IProcess WebActionProcess { get; protected set; }

        /// <summary>
        /// Get the current state of the process model. Safe casted.
        /// </summary>
        public T ProcessModel<T>() where T: class
        {
            return WebActionProcess.Model as T;
        }

        /// <summary>
        /// Gets all menu configurations defined in loaded assemblies.
        /// </summary>
        /// <returns></returns>
        public static IList<IMenuConfiguration> MenuConfigurations()
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            IList<IMenuConfiguration> types = assemblies
                .SelectMany(a => a.GetTypes()
                    .Where(t => !t.IsAbstract)
                    .Where(t => t.GetCustomAttributes(typeof(MenuConfigurationAttribute), true).Any()))
                .Select(t => Activator.CreateInstance(t) as IMenuConfiguration)
                .ToList();

            return types;
        }


        /// <summary>
        /// Finds all process in loaded assemblies.
        /// </summary>
        /// <returns></returns>
        public static IList<Type> GetAllProcesses()
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();

            IList<Type> types = assemblies
                .SelectMany(a => a.GetTypes()
                    .Where(t => !t.IsAbstract)
                    .Where(t => t.GetCustomAttributes(typeof(ProcessAttribute), true).Any()))
                .ToList();

            return types;
        }

        /// <summary>
        /// Get the model of transitions and actions for a view. Must be called from a View Controller
        /// </summary>
        /// <param name="frameSkip"></param>
        /// <returns></returns>
        public ProcessMetaModel GetMetaModel(int frameSkip = 1)
        {
            MethodBase callingMethod = new StackFrame(frameSkip).GetMethod();
            string callingAction = callingMethod.Name;
            Type callingController = callingMethod.DeclaringType;
            
            Type stateToTransitionFrom = FindViewToTransitionFrom(callingController, callingAction);

            IEnumerable<ProcessTransitionIdentifier> apiTransitions = GetProcessTransitionIdentifiers(stateToTransitionFrom);
            ResourceIdentifier view = GetView(stateToTransitionFrom);

            return new ProcessMetaModel
                       {
                           View = view,
                           Transitions = apiTransitions.ToList()
                       };
        }

        /// <summary>
        /// Get a list of transitions for a process state mapped as web action identifiers.
        /// </summary>
        private static IEnumerable<ProcessTransitionIdentifier> GetProcessTransitionIdentifiers(Type stateToTransitionFrom)
        {
            IEnumerable<ProcessTransitionIdentifier> apiTransitions = stateToTransitionFrom
                .GetMethods()
                .Where(method => method.GetCustomAttributes(typeof (TransitionAttribute), true).Any())
                .Select(method => method
                    .GetCustomAttributes(typeof (TransitionAttribute), true)
                    .OfType<TransitionAttribute>()
                    .Select(transition => new ProcessTransitionIdentifier
                        {
                            StereoType = transition.StereoType,
                            Data = GetDataResource(transition),
                            View = GetViewResource(method)
                        })
                    .First());
            return apiTransitions;
        }

        private static ResourceIdentifier GetDataResource(TransitionAttribute transition)
        {
            return new ResourceIdentifier
                       {
                           ActionName = transition.ActionName,
                           AreaName = transition.AreaName,
                           ControllerName = transition.ControllerName
                       };
        }

        private static ResourceIdentifier GetViewResource(MethodInfo method)
        {
            return method
                .ReturnType
                .GetCustomAttributes(typeof (ViewAttribute), true)
                .OfType<ViewAttribute>()
                .Where(view => !(view is CloseTabViewAttribute))
                .Select(view => new ResourceIdentifier
                                    {
                                        ActionName = view.ActionName,
                                        AreaName = view.AreaName,
                                        ControllerName = view.ControllerName
                                    })
                .FirstOrDefault();
        }

        /// <summary>
        /// Get a resource identifier for the view of the state.
        /// </summary>
        /// <param name="stateType">This must be the interface type from the mapping.</param>
        public static ResourceIdentifier GetView(Type stateType)
        {
            IList<ViewAttribute> viewAttributes = stateType
                .GetCustomAttributes(typeof(ViewAttribute), true)
                .OfType<ViewAttribute>()
                .ToList();

            if (viewAttributes.Count > 1)
            {
                throw new Exception(string.Format("State of type '{0}' has multiple views defined", stateType.AssemblyQualifiedName));
            }

            if (viewAttributes.Count < 1)
            {
                throw new Exception(string.Format("State of type '{0}' has no view state defined", stateType.AssemblyQualifiedName));
            }

            ViewAttribute view = viewAttributes.Single();

            return new ResourceIdentifier
            {
                ActionName = view.ActionName,
                AreaName = view.AreaName,
                ControllerName = view.ControllerName
            };
        }

        /// <summary>
        /// Find the type of the state using it's view based on the controller type and action name.
        /// </summary>
        private static Type FindViewToTransitionFrom(Type callingController, string callingAction)
        {
            //Get the view
            foreach (Type process in GetAllProcesses())
            {
                foreach (Type state in process.GetNestedTypes())
                {
                    bool matchingState = state
                        .GetCustomAttributes(typeof (ViewAttribute), true)
                        .OfType<ViewAttribute>()
                        .Any(p => p.ActionName == callingAction && p.ViewController == callingController);

                    if (matchingState)
                    {
                        return state;
                    }
                }
                
            }
            return null;
        }

        /// <summary>
        /// Given a process, a data controller and action name, find a transition method
        /// </summary>
        /// <param name="process"></param>
        /// <param name="callingController"></param>
        /// <param name="callingAction"></param>
        /// <returns></returns>
        private MethodInfo FindTransitionMethod(IProcess process, Type callingController, string callingAction)
        {
            Type stateMapping = GetProcessType(process)
                .GetNestedTypes()
                .Where(type => type.IsInterface)
                .Where(type => type.GetCustomAttributes(typeof(StateAttribute), true).Any())
                .First(type => type.IsInstanceOfType(process.CurrentState)); ;

            foreach (MethodInfo method in stateMapping.GetMethods())
            {
                bool isMatch = method
                    .GetCustomAttributes(typeof (TransitionAttribute), true)
                    .OfType<TransitionAttribute>()
                    .Any(
                        transition =>
                        transition.ActionName == callingAction && transition.ApiController == callingController);

                if (isMatch)
                {
                    return method;
                }
            }

            return null;
        }

        private string AvailableTransitionsForErrorMessage(IProcess process, bool includeTypeDetail = false)
        {
            Type stateMapping = GetProcessType(process)
                .GetNestedTypes()
                .Where(type => type.IsInterface)
                .Where(type => type.GetCustomAttributes(typeof(StateAttribute), true).Any())
                .First(type => type.IsInstanceOfType(process.CurrentState)); ;

            

            string actions = stateMapping
                .GetMethods()
                .Where(method => method.GetCustomAttributes(typeof (TransitionAttribute), true).Any())
                .Select(method =>
                            {
                                var transition = method
                                    .GetCustomAttributes(typeof (TransitionAttribute), true)
                                    .OfType<TransitionAttribute>()
                                    .First();

                                string format = includeTypeDetail
                                                    ? "{0}.{1} -> {2}.{3}"
                                                    : "{1} -> {3}";

                                return string.Format(format, 
                                    transition.ApiController.FullName, 
                                    transition.ActionName,
                                    stateMapping.FullName,
                                    method.Name);
                            })
                .Aggregate((c, n) => string.Concat(c, Environment.NewLine, n));

            return actions;
        }

        /// <summary>
        /// Returns the type of process. This method can be overridden to handle mocked/proxied types.
        /// </summary>
        /// <remarks>
        /// The mock process manager overrides this method to acquire the process type of mocked processes.
        /// </remarks>
        protected virtual Type GetProcessType(IProcess process)
        {
            return process.GetType();
        }

        /// <summary>
        /// Find the process type which will be intialised based on the calling controller.
        /// </summary>
        /// <param name="callingController"></param>
        /// <returns></returns>
        private Type FindProcessForInitialisation(Type callingController)
        {
            //Get the view
            foreach (Type process in GetAllProcesses())
            {
                foreach (Type state in process.GetNestedTypes())
                {
                    bool matchingState = state
                        .GetCustomAttributes(typeof(InitialStateAttribute), true)
                        .OfType<InitialStateAttribute>()
                        .Any(p => p.ApiController == callingController);

                    if (matchingState)
                    {
                        return process;
                    }
                }

            }
            return null;
        }

        /// <summary>
        /// Get's the process meta model for the initial state of the process.
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        public ProcessMetaModel GetProcessIntialiser(Type process)
        {
            Type initialState = GetProcessInitialState(process);
            ResourceIdentifier view = GetView(initialState);
            IEnumerable<ProcessTransitionIdentifier> transitions = GetProcessTransitionIdentifiers(initialState);

            return new ProcessMetaModel
                       {
                           View = view,
                           Transitions = transitions.ToList(),
                           Action = initialState
                               .GetCustomAttributes(typeof (InitialStateAttribute), true)
                               .OfType<InitialStateAttribute>()
                               .Select(state => new ResourceIdentifier
                                                    {
                                                        ActionName = "Index",
                                                        AreaName = state.AreaName,
                                                        ControllerName = state.ControllerName
                                                    })
                               .First(),
                           Description = process
                               .GetCustomAttributes(typeof (ProcessAttribute), true)
                               .OfType<ProcessAttribute>()
                               .First()
                               .Description
                       };
        }

        /// <summary>
        /// Gets the initial state of the process. Throws exception if there is not exactly 1 initial state type.
        /// </summary>
        public Type GetProcessInitialState(Type processType)
        {
            IList<Type> initialStateTypes = processType
                .GetNestedTypes()
                .Where(t => t.GetCustomAttributes(typeof(InitialStateAttribute), true).Any())
                .ToList();

            if (initialStateTypes.Count > 1)
            {
                string initialStateTypeString = initialStateTypes
                    .Select(t => string.Format("'{0}'", t.Name))
                    .Aggregate((c, n) => string.Concat(c, ", ", n));

                throw new Exception(string.Format("Process of type '{0}' has multiple initial states defined. Initial state is set on types: {1}", processType.AssemblyQualifiedName, initialStateTypeString));
            }

            if (initialStateTypes.Count < 1)
            {
                throw new Exception(string.Format("Process of type '{0}' has no initial state defined", processType.AssemblyQualifiedName));
            }

            return initialStateTypes.Single();
        }

        /// <summary>
        /// Performs an initialisation on the process. Create's a process instance, calls the initialisation method and returns the process tab id
        /// </summary>
        public Guid InitialiseProcess()
        {
            MethodBase callingMethod = new StackFrame(1).GetMethod();
            Type callingController = callingMethod.DeclaringType;

            return InitialiseProcess(callingController, callingMethod);
        }

        /// <summary>
        /// Performs an initialisation on the process. Create's a process instance, calls the initialisation method and returns the process tab id
        /// </summary>
        public Guid InitialiseProcess(Type controller, MethodBase action)
        {
            Guid tabid = Guid.NewGuid();

            Type processType = FindProcessForInitialisation(controller);

            IProcess processInstance = CreateProcessInstance(processType);
            
            //TODO: Add make these configurable.
            processInstance.EnvironmentConfiguration.CountryId = 1;
            processInstance.EnvironmentConfiguration.CurrencyId = 162; //ZAR;

            processInstance.Initialise(tabid);
            Processes.Add(tabid, processInstance);
            

            return tabid;
        }

        /// <summary>
        /// Create an instance of the specified process.
        /// </summary>
        /// <param name="processType"></param>
        /// <returns></returns>
        protected virtual IProcess CreateProcessInstance(Type processType)
        {
            IProcess processInstance = (IProcess) Activator.CreateInstance(processType);
            return processInstance;
        }

        /// <summary>
        /// Attach this Process manager to the current web action using the calling method.
        /// </summary>
        public virtual void Attach(Guid tabid, bool isTabRefresh = false)
        {
            WebActionProcess = Processes[tabid];
            SupressTransitions = isTabRefresh;
        }

        /// <summary>
        /// Invoke a transition using the calling method.
        /// </summary>
        /// <returns>The current model state. The type parameter specifies the return type you are expecting.</returns>
        /// <remarks>
        /// This method does nothing when transitions are suppressed.
        /// </remarks>
        public T Transition<T>(object model = null) where T:class
        {
            if (!SupressTransitions)
            {
                MethodBase callingMethod = new StackFrame(1).GetMethod();
                string callingAction = callingMethod.Name;
                Type callingController = callingMethod.DeclaringType;

                if (WebActionProcess.Model != null && !(WebActionProcess.Model is T))
                {
                    throw new Exception("The requested process model type cannot be assigned from the process model");
                }

                MethodInfo transitionAction = FindTransitionMethod(WebActionProcess, callingController, callingAction);
                if (transitionAction == null)
                {
                    string shortMapping = AvailableTransitionsForErrorMessage(WebActionProcess);
                    string longMapping = AvailableTransitionsForErrorMessage(WebActionProcess, true);
                    string description = string.Format("Available Actions from {0}.{1}:{2}{3}",
                                                       WebActionProcess.GetType().Name,
                                                       WebActionProcess.CurrentState.GetType().Name, Environment.NewLine,
                                                       shortMapping);

                    string fullDescription = string.Format("Process: {1}{0}State: {2}{0}Controller Method: {3}.{4}{0}Mappings:{0}{5}", Environment.NewLine,
                                                           WebActionProcess.GetType().FullName, 
                                                           WebActionProcess.CurrentState.GetType().FullName,
                                                           callingController.FullName, callingMethod.Name,
                                                           longMapping);


                    throw new Exception("The requested action cannot be performed at this time.", new Exception(description, new Exception(fullDescription)));
                }

                Type previousStateType = WebActionProcess.CurrentState.GetType();

                WebActionProcess.InvokeTransition(transitionAction, model);

                if (transitionAction.GetCustomAttributes(typeof(TransitionAttribute), true).OfType<TransitionAttribute>().First().StereoType != TransitionStereoType.FileDownload)
                {
                    WebActionProcess.LastCaller = new ProcessEntrantInfo
                                                      {
                                                          Transition = transitionAction,
                                                          StateType = previousStateType
                                                      };
                }
            }
            return (T) WebActionProcess.Model;
        }

        /// <summary>
        /// Gets the process steps for the process and indicates which state is the current state. Defaults to current process.
        /// </summary>
        /// <returns></returns>
        public IList<ProcessStateModel> GetCurrentProcessStateModel(Type state = null)
        {
            if (state == null)
            {
                state = WebActionProcess.CurrentState.GetType();
            }

            IEnumerable<ProcessStateModel> processStateModel = WebActionProcess
                .GetType()
                .GetNestedTypes()
                .Where(p => p.IsInterface)
                .Where(p => typeof (IProcessState).IsAssignableFrom(p))
                .Select(p =>
                            {
                                var stateAttr = p
                                    .GetCustomAttributes(typeof (StateAttribute), true)
                                    .OfType<StateAttribute>()
                                    .FirstOrDefault();

                                return new Tuple<Type, StateAttribute>(p, stateAttr);
                            })
                            .OrderBy(t => t.Item2.Order)
                            .Select(t => new ProcessStateModel
                                             {
                                                 IsCurrentState = t.Item1.IsAssignableFrom(state),
                                                 Name = t.Item2.Label
                                             });
                    

            return processStateModel.ToList();
        }

        /// <summary>
        /// Gets a list of the process tabs and the meta data all processes in memory to allow a resume of the processes.s
        /// </summary>
        public IList<ProcessResumeInfo> GetProcessResumeInfo()
        {
            return Processes
                .Select(p => new ProcessResumeInfo
                                 {
                                     Tabid = p.Key,
                                     Label = GetStateDescription(p.Value),
                                     RefreshStateInfo = GetResumeTransition(p.Value)
                                 })
                .ToList();
        }

        /// <summary>
        /// Determines the last transition which was performed on the given process.
        /// </summary>
        private ProcessMetaModel GetResumeTransition(IProcess process)
        {
            ProcessEntrantInfo lastCaller = process.LastCaller;

            if (lastCaller == null)
            {
                ProcessMetaModel initializer =  GetProcessIntialiser(process.GetType());

                return new ProcessMetaModel
                           {
                               Description = initializer.Description,
                               Action = initializer.Action,
                               View = initializer.View
                           };
            }

            ResourceIdentifier viewIdentifier = GetCurrentView(process);

            ResourceIdentifier actionIdentifier = lastCaller
                .Transition
                .GetCustomAttributes(typeof (TransitionAttribute), true)
                .OfType<TransitionAttribute>()
                .Select(GetDataResource)
                .FirstOrDefault();

            return new ProcessMetaModel
                       {
                           Description = GetProcessDescription(),
                           View = viewIdentifier,
                           Action = actionIdentifier
                       };
        }

        /// <summary>
        /// Returns the resource identifier for the current concrete state of the given process.
        /// </summary>
        /// <param name="process"></param>
        /// <returns></returns>
        public ResourceIdentifier GetCurrentView(IProcess process)
        {
            if (process.CurrentState is ProcessCompletedResult)
            {
                return null;
            }

            ResourceIdentifier viewIdentifier = process
                .GetType()
                .GetNestedTypes()
                .Where(p => p.IsInterface)
                .Where(p => p.GetCustomAttributes(typeof(StateAttribute), true).Any())
                .First(p => p.IsInstanceOfType(process.CurrentState))
                .GetCustomAttributes(typeof(ViewAttribute), true)
                .OfType<ViewAttribute>()
                .Select(p => new ResourceIdentifier
                {
                    ActionName = p.ActionName,
                    AreaName = p.AreaName,
                    ControllerName = p.ControllerName
                })
                .FirstOrDefault();

            return viewIdentifier;
        }

        /// <summary>
        /// Removes the process matching tabId from the session.
        /// </summary>
        /// <param name="tabId"></param>
        public void RemoveProcess(Guid tabId)
        {
            Processes.Remove(tabId);
        }

        /// <summary>
        /// Gets a description of the current or supplied process.
        /// </summary>
        public string GetProcessDescription(IProcess process = null)
        {
            if (process == null)
            {
                process = WebActionProcess;
            }

            string description = process
               .GetType()
               .GetCustomAttributes(typeof(ProcessAttribute), true)
               .OfType<ProcessAttribute>()
               .Select(p => p.Description)
               .FirstOrDefault();

            return description;
        }

        /// <summary>
        /// Get a description of the state for the current process or supplied process.
        /// </summary>
        public string GetStateDescription(IProcess process = null)
        {
            if (process == null)
            {
                process = WebActionProcess;
            }

            string description = process
                .GetType()
                .GetNestedTypes()
                .Where(p => p.IsInterface)
                .First(p => p.IsInstanceOfType(process.CurrentState))
                .GetCustomAttributes(typeof(StateAttribute), true)
                .OfType<StateAttribute>()
                .Select(p => p.Label)
                .FirstOrDefault();

            return description;
        }

        /// <summary>
        /// A helper method which is used to indicate intend for a "redirect" operation on transitions.
        /// </summary>
        /// <remarks>
        /// This method doesn't do anything. It is an artifact to communicate intent.
        /// </remarks>
        public static T RedirectToAction<T>(Func<T> actionForTranstion)
        {
            return actionForTranstion();
        }
    }
}
