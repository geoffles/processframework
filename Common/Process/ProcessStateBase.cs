﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcessFramework.Client.Common.Process
{
    [Serializable]
    public class ProcessStateBase<T> : IProcessState
    {
        /// <summary>
        /// Delegate to access the process model state from the process state implementations.
        /// </summary>
        public ProcessModelAccessor ModelAccessor { get; set; }

        protected T Model
        {
            get { return (T) ModelAccessor.GetModel(); }
        }

        /// <summary>
        /// Executed when entering this state
        /// </summary>
        public void Enter()
        {
            OnEnter();
        }

        /// <summary>
        /// Override this method to perform something when entering this state
        /// </summary>
        protected virtual void OnEnter(){}
    }
}
