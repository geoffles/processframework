using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to contain environment configurations for processes.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-11-04<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    [Serializable]
    public sealed class EnvironmentConfiguration
    {
        public const string COUNTRY_ID_KEY = "COUNTRY_ID";
        public const string CURRENCY_ID_KEY = "CURRENCY_ID";

        public EnvironmentConfiguration()
        {
            Settings = new Dictionary<string, object>();
        }

        public IDictionary<string, object> Settings { get; private set; }

        public int CountryId
        {
            get { return (int)Settings[COUNTRY_ID_KEY]; }
            set { Settings[COUNTRY_ID_KEY] = value; }
        }

        public int CurrencyId
        {
            get { return (int)Settings[CURRENCY_ID_KEY]; }
            set { Settings[CURRENCY_ID_KEY] = value; }
        }
    }
}
