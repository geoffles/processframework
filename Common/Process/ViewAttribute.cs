﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to provide a class decoration to indicate that the class represents a process state that has a view mapping
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2012-06-26<br/>
    /// </remarks>
    /// </summary>
    [AttributeUsage(AttributeTargets.Interface|AttributeTargets.Class)]
    public class ViewAttribute : Attribute
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        protected ViewAttribute(){}

        /// <summary>
        /// The type of of the view controller
        /// </summary>
        public Type ViewController { get; private set; }

        /// <summary>
        /// The name of the web action to fetch the view
        /// </summary>
        public string ActionName { get; private set; }


        /// <summary>
        /// The name of the controller to fetch the view from. Sourced from ViewController.
        /// </summary>
        public string ControllerName
        {
            get
            {
                return ViewController
                    .GetFields(BindingFlags.Static | BindingFlags.Public)
                    .Single(f => f.Name == "NameConst")
                    .GetRawConstantValue()
                    .ToString();
            }
        }

        /// <summary>
        /// The name of the controller's area. Sourced from ViewController.
        /// </summary>
        public string AreaName
        {
            get
            {
                return ViewController
                    .GetFields(BindingFlags.Static | BindingFlags.Public)
                    .Single(f => f.Name == "AreaConst")
                    .GetRawConstantValue()
                    .ToString();
            }
        }

        /// <summary>
        /// Create a new instance of a view attribute.
        /// </summary>
        /// <param name="viewController">The view controller type</param>
        /// <param name="actionName">The action name for the view</param>
        public ViewAttribute(Type viewController, string actionName)
        {
            ViewController = viewController;
            ActionName = actionName;
        }
    }
}
