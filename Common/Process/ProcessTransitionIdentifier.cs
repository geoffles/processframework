﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to provide information regarding a client view and it's transitions.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2012-06-26<br/>
    /// </remarks>
    /// </summary>
    public class ProcessTransitionIdentifier
    {
        /// <summary>
        /// The view for the state transitioned to.
        /// </summary>
        public ResourceIdentifier View { get; set; }

        /// <summary>
        /// The data for the state transitioned to.
        /// </summary>
        public ResourceIdentifier Data { get; set; }
        
        /// <summary>
        /// The stereo type for this transition
        /// </summary>
        public TransitionStereoType StereoType { get; set; }
    }
}
