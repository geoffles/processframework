﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to indicate that the process is now complete.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2012-06-26<br/>
    /// </remarks>
    /// </summary>
    [CloseTabView]
    [Serializable]
    public class ProcessCompletedResult : IProcessState
    {
        /// <summary>
        /// Delegate to get the model.
        /// </summary>
        public ProcessModelAccessor ModelAccessor { get; set; }

        /// <summary>
        /// Executed when entering this state
        /// </summary>
        public void Enter(){}

        public bool WrapAroundProcess { get; set; }
    }
}
