using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessFramework.Client.Common.Process.Menu
{
    /// <summary>
    /// The responsibility of this class is to provide an interface for extraction of menu configuration data.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-09-03<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public interface IMenuConfiguration
    {
        /// <summary>
        /// Gets the process groups for the LHS menu groups
        /// </summary>
        IList<ProcessGroup> GetMenuGroups();

        /// <summary>
        /// Gets the processes for the LHS menu items
        /// </summary>
        IList<ProcessGrouping> GetMenuItems();
    }
}
