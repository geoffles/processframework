using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessFramework.Client.Common.Process.Menu
{
    /// <summary>
    /// The responsibility of this class is to decorate classes so that 
    /// they can be gathered for the purposes of menu configuration.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-09-03<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class MenuConfigurationAttribute : Attribute
    {
    }
}
