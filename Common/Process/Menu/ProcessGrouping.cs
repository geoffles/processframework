using System;

namespace ProcessFramework.Client.Common.Process.Menu
{
    /// <summary>
    /// The responsibility of this class is to provide the ordering and grouping information for a process for the LHS Menu.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-09-03<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public sealed class ProcessGrouping
    {
        /// <summary>
        /// The preferred order of appearance for this item
        /// </summary>
        public int Order { get; set; }
        
        /// <summary>
        /// Key for the group which this process item will belong to
        /// </summary>
        public string GroupKey { get; set; }

        /// <summary>
        /// The meta model for the process.
        /// </summary>
        public Type Process { get; set; }

        /// <summary>
        /// The menu label.
        /// </summary>
        public string Label { get; set; }
    }
}
