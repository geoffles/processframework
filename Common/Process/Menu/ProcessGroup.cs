namespace ProcessFramework.Client.Common.Process.Menu
{
    /// <summary>
    /// The responsibility of this class is to represent a menu group (folding item)
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-09-03<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public sealed class ProcessGroup
    {
        /// <summary>
        /// The key for this group
        /// </summary>
        public string GroupKey { get; set; }

        /// <summary>
        /// The Label for this group (as displayed on the LHS Menu).
        /// </summary>
        public string Label { get; set; }
    }
}
