﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Http.Routing;
using UrlHelper = System.Web.Mvc.UrlHelper;
using HttpUrlHelper = System.Web.Http.Routing.UrlHelper;

namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to provide data for the views which indicates available transitions for the view.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2012-06-26<br/>
    /// </remarks>
    /// </summary>
    public class ProcessMetaModel
    {
        /// <summary>
        /// Helper method to get the relative URL for the view of the stereo type (eg, the next view URL)
        /// </summary>
        public string ViewFor(TransitionStereoType stereoType, UrlHelper url)
        {
            return string.Empty;
            ProcessTransitionIdentifier transition = Transitions.FirstOrDefault(p => p.StereoType == stereoType);
            return url.Action(transition.View.ActionName, new { controller = transition.View.ControllerName, area = transition.View.AreaName });
        }

        /// <summary>
        /// Helper method to get the relative URL for the api action of the stereo type (eg, the next transition action URL)
        /// </summary>
        public string DataFor(TransitionStereoType stereoType, UrlHelper url)
        {
            ProcessTransitionIdentifier transition = Transitions.FirstOrDefault(p => p.StereoType == stereoType);

            return url.RouteUrl("DefaultApi",
                                new
                                    {
                                        httproute = "",
                                        area = transition.Data.AreaName,
                                        controller = transition.Data.ControllerName,
                                        action = transition.Data.ActionName
                                    });
        }

        /// <summary>
        /// Helper method to resolve an API action
        /// </summary>
        public string ApiAction(string actionName, string controllerName, string areaName, UrlHelper url)
        {
            return url.RouteUrl("DefaultApi", new { httproute = "", area = areaName, controller = controllerName, action = actionName });
        }

        /// <summary>
        /// Helper method to resolve an API action
        /// </summary>
        public string ApiAction(ResourceIdentifier identifier, UrlHelper url)
        {
            return url.RouteUrl("DefaultApi", new { httproute = "", area = identifier.AreaName, controller = identifier.ControllerName, action = identifier.ActionName });
        }

        public string ToAction(UrlHelper url)
        {
            return ApiAction(Action.ActionName, Action.ControllerName, Action.AreaName, url);
        }

        public string ToView(UrlHelper url)
        {
            return url.Action(View.ActionName, new { controller = View.ControllerName, area = View.AreaName });
        }

        /// <summary>
        /// The list of transitions which can be made from the view.
        /// </summary>
        public List<ProcessTransitionIdentifier> Transitions { get; set; }

        /// <summary>
        /// The process' current view's resource identifier
        /// </summary>
        public ResourceIdentifier View { get; set; }

        /// <summary>
        /// This view's data resource identifier. This is used only for process initialisation.
        /// </summary>
        public ResourceIdentifier Action { get; set; }

        /// <summary>
        /// The processes' description of the process.
        /// </summary>
        public string Description { get; set; }
    }
}
