using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Mvc;


namespace ProcessFramework.Client.Common.Process
{
    /// <summary>
    /// The responsibility of this class is to create the download response
    /// <remarks>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-09-20<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public static class FileDownloadHelper
    {        
        private const string TextContentType = "text/plain";

        public static HttpResponseMessage CreateFileDownloadResponseMessage(MemoryStream fileStream, string filename)
        {
            HttpResponseMessage response = new HttpResponseMessage()
            {
                Content = new StreamContent(fileStream),
                StatusCode = HttpStatusCode.OK
            };

            response.Content.Headers.ContentType = new MediaTypeHeaderValue(TextContentType);
            response.Content.Headers.Add("Set-Cookie", string.Format("fileDownload={0}; path=/", ProcessManager.Instance.WebActionProcess.TabId));
            
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("Attachment")
                                                                {
                                                                    FileName = filename
                                                                };

            return response;
        }
    }
}
