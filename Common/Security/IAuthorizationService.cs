﻿using System;

using ProcessFramework.Client.Common.Constants;

namespace ProcessFramework.Client.Common.Security
{
    /// <summary>
    /// The contract defining the authorization service.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public interface IAuthorizationService
    {
        /// <summary>
        /// Gets the user permissions.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        Permission GetUserPermissions(string username);
    }
}
