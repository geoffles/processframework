﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Linq;

using ProcessFramework.Client.Common.Config;
using ProcessFramework.Client.Common.Constants;

using log4net;

namespace ProcessFramework.Client.Common.Security
{
    /// <summary>
    /// The responsibility of this class is to coordinate authorization based on settings in the Config/pfx.security.config file.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public class FileAuthorizationService : IAuthorizationService
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(FileAuthorizationService));

        #region IAuthorizationService Members

        /// <summary>
        /// Gets the principal permissions.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        public Permission GetUserPermissions(string username)
        {
            Permission userPermissionGroup = default(Permission);
            SecurityConfigurationSection security = SecurityConfigurationSection.Security;
            for(int index = 0; index < security.PermissionGroups.Count; index++)
            {
                PermissionGroupElement permissionGroupElement = security.PermissionGroups[index];
                for(int userIndex = 0; userIndex < permissionGroupElement.Users.Count; userIndex++)
                {
                    UserElement userElement = permissionGroupElement.Users[userIndex];
                    bool isUserInPermissionGroup = string.CompareOrdinal(username.ToLowerInvariant(), userElement.Name.ToLowerInvariant()) == 0;
                    if(!isUserInPermissionGroup)
                    {
                        isUserInPermissionGroup = IsUserInActiveDirectoryGroup(username, userElement.Name);
                    }
                    if(isUserInPermissionGroup)
                    {
                        List<Permission> permissionGroups = GetPermissions(permissionGroupElement.GroupName).ToList();
                        permissionGroups.ForEach(pgroup => userPermissionGroup |= pgroup);
                        break;
                    }
                }
            }
            return userPermissionGroup;
        }

        #endregion

        private static bool IsUserInActiveDirectoryGroup(string username, string adGroup)
        {
            bool isUserInActiveDirectoryGroup = false;
            
            string dnsHostName = ConfigurationManager.AppSettings[AppSettingsKey.DnsHostName];
            PrincipalContext context;
            if(string.IsNullOrEmpty(dnsHostName))
            {
                context = new PrincipalContext(ContextType.Domain);
            }
            else
            {
                context = new PrincipalContext(ContextType.Domain, dnsHostName);
            }

            UserPrincipal upUser = UserPrincipal.FindByIdentity(context, username);
            if(upUser != null)
            {
                GroupPrincipal groupPrincipal = GroupPrincipal.FindByIdentity(context, adGroup);
                if(groupPrincipal != null)
                {
                    try
                    {
                        isUserInActiveDirectoryGroup = upUser.IsMemberOf(groupPrincipal);
                    }
                    catch(Exception ex)
                    {
                        if(Log.IsErrorEnabled)
                        {
                            Log.Error("Error occurred while checking user membership.", ex);
                        }
                    }
                }
            }
            return isUserInActiveDirectoryGroup;
        }

        private static IEnumerable<Permission> GetPermissions(string groupNames)
        {
            return groupNames.Split(',').Select(groupName => (Permission) Enum.Parse(typeof(Permission), groupName));
        }

        private static ILog Log
        {
            get { return _log; }
        }
    }
}
