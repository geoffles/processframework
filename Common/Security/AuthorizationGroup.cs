﻿using System;
using System.Collections.Generic;
using System.Security.Principal;

namespace ProcessFramework.Client.Common.Security
{
    /// <summary>
    /// The responsibility of this class is to represent a Funding authorization group.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public class AuthorizationGroup
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets the identities.
        /// </summary>
        public IList<IIdentity> Identities { get; set; }
    }
}
