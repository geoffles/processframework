﻿using System;
using System.Security.Principal;

using ProcessFramework.Client.Common.Constants;

namespace ProcessFramework.Client.Common.Security
{
    /// <summary>
    /// The responsibility of this class is to represent a ProcessFramework identity.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    [Serializable]
    public class ProcessFrameworkIdentity : IIdentity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessFrameworkIdentity"/> class.
        /// </summary>
        /// <param name="identity">The identity.</param>
        public ProcessFrameworkIdentity(IIdentity identity)
        {
            Identity = identity;
        }

        private IIdentity Identity { get; set; }

        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        /// <value>
        /// The display name.
        /// </value>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the permissions.
        /// </summary>
        /// <value>
        /// The permissions.
        /// </value>
        public Permission? Permission { get; set; }

        /// <summary>
        /// Gets or sets the email address.
        /// </summary>
        /// <value>
        /// The email address.
        /// </value>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Gets or sets the name of the given.
        /// </summary>
        /// <value>
        /// The name of the given.
        /// </value>
        public string GivenName { get; set; }

        /// <summary>
        /// Gets or sets the surname.
        /// </summary>
        /// <value>
        /// The surname.
        /// </value>
        public string Surname { get; set; }

        /// <summary>
        /// Gets or sets the employee id.
        /// </summary>
        /// <value>
        /// The employee id.
        /// </value>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the voice telephone number.
        /// </summary>
        /// <value>
        /// The voice telephone number.
        /// </value>
        public string VoiceTelephoneNumber { get; set; }

        #region IIdentity Members

        /// <summary>
        /// Gets the type of authentication used.
        /// </summary>
        /// <returns>The type of authentication used to identify the user.</returns>
        public string AuthenticationType
        {
            get { return Identity.AuthenticationType; }
        }

        /// <summary>
        /// Gets a value that indicates whether the user has been authenticated.
        /// </summary>
        /// <returns>true if the user was authenticated; otherwise, false.</returns>
        public bool IsAuthenticated
        {
            get { return Identity.IsAuthenticated; }
        }

        /// <summary>
        /// Gets the name of the current user.
        /// </summary>
        /// <returns>The name of the user on whose behalf the code is running.</returns>
        public string Name
        {
            get
            {
                return Identity.Name;
            }
        }

        #endregion
    }
}
