﻿using System;
using System.Security.Principal;

using ProcessFramework.Client.Common.Constants;

namespace ProcessFramework.Client.Common.Security
{
    /// <summary>
    /// The responsibility of this class is to represent the context of the logged in user.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    [Serializable]
    public class WebContext
    {
        /// <summary>
        /// Gets or sets the principal.
        /// </summary>
        /// <value>
        /// The principal.
        /// </value>
        public ProcessFrameworkPrincipal Principal { get; set; }

        /// <summary>
        /// Determines whether user (principal) is in permission group.
        /// </summary>
        /// <param name="identity">The identity.</param>
        /// <param name="permissionGroup">The permission group.</param>
        /// <returns>
        ///   <c>true</c> if the user in the permission group; otherwise, <c>false</c>.
        /// </returns>
        public bool HasPermissions(IIdentity identity, Permission permissionGroup)
        {
            bool isUserInPermissionGroup = ((Principal.ProcessFrameworkIdentity.Permission & permissionGroup) == permissionGroup);
            return isUserInPermissionGroup;
        }
    }
}
