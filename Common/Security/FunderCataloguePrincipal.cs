﻿using System;
using System.Security.Principal;

using ProcessFramework.Client.Common.Constants;

namespace ProcessFramework.Client.Common.Security
{
    /// <summary>
    /// The responsibility of this class is to represent the ProcessFramework principal.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    [Serializable]
    public class ProcessFrameworkPrincipal : IPrincipal
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessFrameworkPrincipal"/> class.
        /// </summary>
        /// <param name="identity">The identity.</param>
        public ProcessFrameworkPrincipal(ProcessFrameworkIdentity identity)
        {
            Identity = identity;
        }

        #region IPrincipal Members

        /// <summary>
        /// Gets the identity of the current principal.
        /// </summary>
        /// <returns>The <see cref="T:System.Security.Principal.IIdentity"/> object associated with the current principal.</returns>
        public IIdentity Identity { get; set; }

        /// <summary>
        /// Gets the funder catalogue identity.
        /// </summary>
        public ProcessFrameworkIdentity ProcessFrameworkIdentity
        {
            get { return (ProcessFrameworkIdentity)Identity; }
        }

        /// <summary>
        /// Determines whether the current principal belongs to the specified role.
        /// </summary>
        /// <param name="role">The name of the role for which to check membership.</param>
        /// <returns>
        /// true if the current principal is a member of the specified role; otherwise, false.
        /// </returns>
        public bool IsInRole(string role)
        {
            bool isInRole = false;
            Permission rolePermission;
            if(Enum.TryParse(role, true, out rolePermission))
            {
                isInRole = ((ProcessFrameworkIdentity.Permission & rolePermission) == rolePermission);
            }
            return isInRole;
        }

        #endregion
    }
}
