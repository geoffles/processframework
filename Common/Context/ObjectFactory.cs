﻿using System;

using ProcessFramework.Client.Common.IoC;

namespace ProcessFramework.Client.Common.Context
{
    /// <summary>
    /// The responsibility of this class is to provide the Spring application context.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public static class ObjectFactory
    {
        static IContainer _container;

        /// <summary>
        /// Gets the container.
        /// </summary>
        public static IContainer Container
        {
            get { return _container?? (_container = new SpringApplicationContextContainer()); }
            set { _container = value; }
        }

        /// <summary>
        /// Returns a boolean value if the current application context contains an named object.
        /// </summary>
        /// <param name="objectName">Accepts the name of the object to check.</param>
        public static bool Contains(string objectName)
        {
            return Container.Contains(objectName);
        }

        /// <summary>
        /// Determines whether the container contains the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        ///   <c>true</c> if [contains] [the specified type]; otherwise, <c>false</c>.
        /// </returns>
        internal static bool Contains(Type type)
        {
            return Container.Contains(type);
        }

        /// <summary>
        /// Return a instance of an object in the context by the specified name.
        /// </summary>
        /// <param name="objectName">Accepts a string object name.</param>
        public static object Resolve(string objectName)
        {
            return Container.Resolve(objectName);
        }

        /// <summary>
        /// Return a instance of an object in the context by the specified name and type.
        /// </summary>
        /// <typeparam name="T">Accepts the type of the object to resolve.</typeparam>
        /// <param name="objectName">Accepts a string object name.</param>
        public static T Resolve<T>(string objectName)
        {
            return Container.Resolve<T>(objectName);
        }

        /// <summary>
        /// Resolves the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static object Resolve(Type type)
        {
            return Container.Resolve(type);
        }

        /// <summary>
        /// Resolves an instance of type <c>T</c>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T Resolve<T>()
        {
            return Container.Resolve<T>();
        }
    }
}
