using System;

namespace ProcessFramework.Client.Common.Attributes
{
    /// <summary>
    /// The responsibility of this class is to indicate property to be ignored when marshalling data to/from Excel.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-10-28<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public sealed class ExcelIgnoreAttribute : Attribute
    {}
}
