using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ProcessFramework.Client.Common.Attributes
{
    /// <summary>
    /// The responsibility of this class is to indicate whether property should be validated based on a property value condition.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-10-28<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public sealed class ValidateIfEqualAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ValidateIfEqualAttribute"/> class.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="valueToCompare">The value to compare.</param>
        public ValidateIfEqualAttribute(string propertyName, object valueToCompare)
        {
            PropertyName = propertyName;
            ValueToCompare = valueToCompare;
        }

        /// <summary>
        /// Gets or sets the name of the property.
        /// </summary>
        /// <value>The name of the property.</value>
        public string PropertyName { get; set; }

        /// <summary>
        /// Gets or sets the value to compare.
        /// </summary>
        /// <value>The value to compare.</value>
        public object ValueToCompare { get; set; }

        /// <summary>
        /// Determines if the property identified by <see cref="PropertyName"/> on <paramref name="instance"/> is
        /// equal to the value specified in <see cref="ValueToCompare"/>.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <returns><c>true</c> if the property identified by <see cref="PropertyName"/> on <paramref name="instance"/> is
        /// equal to the value specified in <see cref="ValueToCompare"/>; <c>false</c> otherwise</returns>
        public bool Evaluate(object instance)
        {
            if (instance == null)
            {
                return false;
            }
            var entityType = instance.GetType();
            var getPropertyInfo = entityType.GetProperty(PropertyName);
            var firstValueToCompare = getPropertyInfo.GetValue(instance, BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.Public, null, null,
                                                               null);

            return Equals(firstValueToCompare, ValueToCompare);
        }
    }
}
