﻿using System;
using System.Web.Mvc;

using ProcessFramework.Client.Common.Context;
using ProcessFramework.Client.Common.Helpers;
using ProcessFramework.Client.Common.Security;

namespace ProcessFramework.Client.Common.Attributes
{
    /// <summary>
    /// The action filter to populate user permissions.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public class PopulateUserPermissionsFilterAttribute : ActionFilterAttribute
    {
        private static WebContext WebContext
        {
            get { return ObjectFactory.Resolve<WebContext>(); }
        }

        private static IAuthorizationService AuthorizationService
        {
            get { return ObjectFactory.Resolve<IAuthorizationService>(); }
        }

        /// <summary>
        /// Called by the ASP.NET MVC framework before the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            SecurityHelper.EnsureUserPermissionsLoaded(filterContext, WebContext, AuthorizationService);
        }
    }
}
