﻿using System;
using System.Diagnostics;
using System.Security.Principal;
using System.Web.Mvc;

using ProcessFramework.Client.Common.Constants;
using ProcessFramework.Client.Common.Context;
using ProcessFramework.Client.Common.Helpers;
using ProcessFramework.Client.Common.Security;

namespace ProcessFramework.Client.Common.Attributes
{
    /// <summary>
    /// The attribute to authorize access to controller actions.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-29<br/>
    /// Details     : Added in a call to the base authorisation behaviour, and changed the handling of the "unauthorised"
    ///               state to use the base behaviour as well.<br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public class ProcessFrameworkAuthorizeAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Gets the required Permission.
        /// </summary>
        public Permission Required { get; set; }

        private static WebContext WebContext
        {
            get { return ObjectFactory.Resolve<WebContext>(); }
        }

        private static IAuthorizationService AuthorizationService
        {
            get { return ObjectFactory.Resolve<IAuthorizationService>(); }
        }

        /// <summary>
        /// Called by the ASP.NET MVC framework before the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (base.AuthorizeCore(filterContext.HttpContext))
            {
                IPrincipal user = filterContext.HttpContext.User;
                if (user == null)
                {
                    Debugger.Break();
                }
                else
                {
                    SecurityHelper.EnsureUserPermissionsLoaded(filterContext, WebContext, AuthorizationService);
                    if (!WebContext.HasPermissions(user.Identity, Required))
                    {
                        base.HandleUnauthorizedRequest(filterContext);
                    }
                }
            }
        }
    }
}
