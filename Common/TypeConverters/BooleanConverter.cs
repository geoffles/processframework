using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;

namespace ProcessFramework.Client.Common.TypeConverters
{
    /// <summary>
    /// The responsibility of this class is to provide an specialisation of the standard <see cref="System.ComponentModel.BooleanConverter"/>
    /// which is capable of handling int-to-bool conversion.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-12-27<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public sealed class BooleanConverter : System.ComponentModel.BooleanConverter
    {
        /// <summary>
        /// Gets a value indicating whether this converter can convert an object in the given source type to a Boolean object using the specified context.
        /// </summary>
        /// <returns><c>true</c> if this object can perform the conversion; otherwise, <c>false</c>.</returns>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext"/> that provides a format context.</param>
        /// <param name="sourceType">A <see cref="T:System.Type"/> that represents the type you wish to convert from.</param>
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(int) || sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        /// <summary>
        /// Converts the given value object to a Boolean object.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Object"/> that represents the converted <paramref name="value"/>.
        /// </returns>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext"/> that provides a format context. </param>
        /// <param name="culture">A <see cref="T:System.Globalization.CultureInfo"/> that specifies the culture to which to convert.</param>
        /// <param name="value">The <see cref="T:System.Object"/> to convert. </param>
        /// <exception cref="T:System.FormatException"><paramref name="value"/> is not a valid value for the target type. </exception>
        /// <exception cref="T:System.NotSupportedException">The conversion cannot be performed. </exception>
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            int intValue;
            if (!(value is int))
            {
                string valueAsString = value as string;
                if (valueAsString != null)
                {
                    if (new List<string> {"y", "yes"}.Contains(valueAsString.ToLower(culture)))
                    {
                        valueAsString = "1";
                    }
                    else if (new List<string> {"n", "no"}.Contains(valueAsString.ToLower(culture)))
                    {
                        valueAsString = "0";
                    }
                    try
                    {
                        object convertedValue = TypeDescriptor.GetConverter(typeof(int)).ConvertFrom(valueAsString);
                        if (convertedValue != null)
                        {
                            intValue = (int) convertedValue;
                        }
                        else
                        {
                            return base.ConvertFrom(context, culture, value);
                        }
                    }
                    catch (Exception ex)
                    {
                        return base.ConvertFrom(context, culture, value);
                    }
                }
                else
                {
                    return base.ConvertFrom(context, culture, value);
                }
            }
            else
            {
                intValue = ((int) value);
            }
            return intValue != 0;
        }
    }
}
