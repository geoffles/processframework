using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProcessFramework.Client.Common.Process;

namespace ProcessFramework.Client.Common.Validation
{
    /// <summary>
    /// The responsibility of this class is to be the singleton that manages the publishing of validation errors.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>    
    /// </remarks>
    /// </summary>
    public sealed class ValidationManager
    {
        private ValidationManager()
        {
        }

        private IProcess _process;
        private IList<ProcessValidationError> ValidationErrors
        {
            get { return _process == null ? null : _process.ValidationErrors; }
        }
            
            
        [ThreadStatic] private static volatile ValidationManager _instance;
        public static ValidationManager Instance
        {
            get { return _instance ?? (_instance = new ValidationManager()); }
        }

        public void Attach(IProcess process)
        {
            _process = process;
        }
        
        public void AddProcessValidationError(ProcessValidationError error)
        {
            if (ValidationErrors == null)
            {
                throw new Exception("The validation manager is not attached to a process, or the process' error list is null.");
            }

            ValidationErrors.Add(error);
        }

        public IList<ProcessValidationError> GetErrors()
        {
            return ValidationErrors;
        }
    }
}
