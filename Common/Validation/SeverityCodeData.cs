namespace ProcessFramework.Client.Common.Validation
{
    public enum SeverityCodeData
    {
        Low = 0,
        Medium = 1,
        High = 2
    }
}
