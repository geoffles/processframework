using System;

namespace ProcessFramework.Client.Common.Validation
{
    /// <summary>
    /// The responsibility of this class is to represent a validation failure that interrupts
    /// process transitioning as an exception.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2014-01-16<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public class ValidationFailedException : Exception
    {}
}