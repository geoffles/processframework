using System;
using System.Runtime.Serialization;
using ProcessFramework.Client.Common.Process;

namespace ProcessFramework.Client.Common.Validation
{
    [Serializable]
    [DataContract]
    public class ProcessValidationError
    {
        /// <summary>
        /// Gets or sets the message identifier to be translated on the client.
        /// </summary>
        [DataMember]
        public string Message { get; set; }


        /// <summary>
        /// Gets or sets the severity of the error.
        /// </summary>
        [DataMember]
        public SeverityCodeData Severity { get; set; }
        
        /// <summary>
        /// Gets or sets a key which identifies this validation error uniquely.
        /// </summary>
        [DataMember]
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the process step to which this error is relevant.
        /// </summary>
        [DataMember]
        public string ProcessStep { get; set; }
    }
}
