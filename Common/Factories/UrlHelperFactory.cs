using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProcessFramework.Client.Common.Factories
{
    /// <summary>
    /// The responsibility of this class is to allow spring to create a URL helper object that can be used in common libraries.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-12-02<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public sealed class UrlHelperFactory
    {
        /// <summary>
        /// Get an instance of the MVC url Helper.
        /// </summary>
        public UrlHelper GetInstance()
        {
            var ctx = new HttpContextWrapper(HttpContext.Current);
            return new UrlHelper(new RequestContext(ctx, RouteTable.Routes.GetRouteData(ctx)));
        }
    }
}
