﻿using System;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Principal;
using System.Web;

using ProcessFramework.Client.Common.Cache;
using ProcessFramework.Client.Common.Helpers;
using ProcessFramework.Client.Common.Security;

using log4net;

//using ProcessFramework.Client.Common.Cache;

namespace ProcessFramework.Client.Common.Factories
{
    /// <summary>
    /// The factory class for creating user web context instances.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    [Serializable]
    public class WebContextFactory
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(WebContextFactory));

        /// <summary>
        /// Initializes a new instance of the <see cref="WebContextFactory"/> class.
        /// </summary>
        /// <param name="authorizationService"></param>
        /// <param name="cacheCoordinator">The cache coordinator.</param>
        public WebContextFactory(IAuthorizationService authorizationService, ICacheCoordinator cacheCoordinator)
        {
            AuthorizationService = authorizationService;
            CacheCoordinator = cacheCoordinator;
        }

        /// <summary>
        /// Creates a user context.
        /// </summary>
        /// <returns></returns>
        public WebContext GetInstance()
        {
            try
            {
                string username = HttpContext.Current.User.Identity.Name;
                string sessionId = HttpContext.Current.Session.SessionID;
                string webContextCacheKey = string.Format("{{{0}}}-{{{1}}}", sessionId, username);
                byte[] webContextByteArray = CacheCoordinator[webContextCacheKey] as byte[];
                WebContext webContext;
                if (webContextByteArray == null)
                {
                    var identity = HttpContext.Current.User.Identity.DeepCopy();
                    webContext = CreateWebContext(identity);
                    SecurityHelper.GetUserPermissionsFromAuthorizationService(webContext, AuthorizationService);
                    using (var ms = new MemoryStream())
                    {
                        var formatter = new BinaryFormatter();
                        formatter.Serialize(ms, webContext);
                        ms.Position = 0;
                        CacheCoordinator[webContextCacheKey] = ms.GetBuffer();
                    }
                }
                else
                {
                    using (var ms = new MemoryStream(webContextByteArray, false))
                    {
                        var formatter = new BinaryFormatter();
                        ms.Position = 0;
                        webContext = (WebContext) formatter.Deserialize(ms);
                    }
                }
                return webContext;
            }
            catch (Exception ex)
            {
                if (Log.IsErrorEnabled)
                {
                    Log.Error("Failed retrieving web context instance.", ex);
                }
                if (ex is TargetInvocationException)
                {
                    if ((ex as TargetInvocationException).InnerException is ArgumentException)
                    {
                        // identity has become stale - rebuild
                        string username = HttpContext.Current.User.Identity.Name;
                        string sessionId = HttpContext.Current.Session.SessionID;
                        string webContextCacheKey = string.Format("{{{0}}}-{{{1}}}", sessionId, username);
                        var identity = HttpContext.Current.User.Identity.DeepCopy();
                        WebContext webContext = CreateWebContext(identity);
                        SecurityHelper.GetUserPermissionsFromAuthorizationService(webContext, AuthorizationService);
                        using (var ms = new MemoryStream())
                        {
                            var formatter = new BinaryFormatter();
                            formatter.Serialize(ms, webContext);
                            ms.Position = 0;
                            CacheCoordinator[webContextCacheKey] = ms.GetBuffer();
                        }
                        return webContext;
                    }
                }
                throw;
            }
        }

        private IAuthorizationService AuthorizationService { get; set; }
        private ICacheCoordinator CacheCoordinator { get; set; }

        private static WebContext CreateWebContext(IIdentity identity)
        {
            var webContext = new WebContext();

            //The otherwise would be AD identity.
            var processFrameworkIdentity =
                                new ProcessFrameworkIdentity(identity)
                                {
                                    DisplayName = "TestUser",
                                    EmailAddress = "noone@nowhere.com",
                                    GivenName = "Test",
                                    Surname = "User",
                                    EmployeeId = "S0ME0N3",
                                    VoiceTelephoneNumber = "000111000",
                                };
            webContext.Principal = new ProcessFrameworkPrincipal(processFrameworkIdentity);
           
            return webContext;
        }

        private static ILog Log
        {
            get { return _log; }
        }

    }
}
