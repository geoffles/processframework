﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel.Description;

namespace ProcessFramework.Client.Common.Helpers
{
    /// <summary>
    /// This class provides utility methods to get and set the application 
    /// and user username/password combinations stored in ClientCredentials.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    internal static class CredentialHelper
    {
        const string CREDENTIAL_DELIMITER = "|=|";

        /// <summary>
        /// Gets the user names.
        /// </summary>
        /// <param name="credentials">The credentials.</param>
        /// <returns></returns>
        public static UserNames GetUserNames(ClientCredentials credentials)
        {
            if (credentials.UserName.UserName == null)
            {
                return new UserNames();
            }

            string[] userNames = credentials.UserName.UserName.Split(new[] {CREDENTIAL_DELIMITER}, StringSplitOptions.RemoveEmptyEntries);
            return new UserNames
                {
                    ApplicationUserName = userNames.Length > 0 ? userNames[0] : null,
                    UserUserName = userNames.Length > 1 ? userNames[1] : null
                };
        }

        /// <summary>
        /// Gets the passwords.
        /// </summary>
        /// <param name="credentials">The credentials.</param>
        /// <returns></returns>
        public static Passwords GetPasswords(ClientCredentials credentials)
        {
            if (credentials.UserName.Password == null)
            {
                return new Passwords();
            }

            var password = credentials.UserName.Password;
            string[] results = password.Split(new[] {CREDENTIAL_DELIMITER}, StringSplitOptions.RemoveEmptyEntries);
            return new Passwords
                {
                    ApplicationPassword = results.Length > 0 ? results[0] : null,
                };
        }

        public static void SetUserNames(ClientCredentials credentials, UserNames userNames)
        {
            credentials.UserName.UserName =
                string.Format("{0}{2}{1}", userNames.ApplicationUserName, userNames.UserUserName, CREDENTIAL_DELIMITER);
        }

        public static void SetPasswords(ClientCredentials credentials, Passwords passwords)
        {
            credentials.UserName.Password =
                string.Format("{0}", passwords.ApplicationPassword);
        }
    }

    /// <summary>
    /// This class provides the structure for the composite 
    /// user name stored in ClientCredentials.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// </remarks>
    /// </summary>
    [DataContract]
    internal sealed class UserNames
    {
        [DataMember] public string ApplicationUserName;

        [DataMember] public string UserUserName;
    }

    /// <summary>
    /// This class provides the structure for the composite 
    /// password stored in ClientCredentials.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// </remarks>
    /// </summary>
    [DataContract]
    internal sealed class Passwords
    {
        [DataMember] public string ApplicationPassword;
    }
}
