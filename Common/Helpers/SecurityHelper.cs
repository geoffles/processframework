﻿using System;
using System.Web.Mvc;

using ProcessFramework.Client.Common.Constants;
using ProcessFramework.Client.Common.Security;

namespace ProcessFramework.Client.Common.Helpers
{
    /// <summary>
    /// The responsibility of this class is to provide helper methods for security related concerns.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public static class SecurityHelper
    {
        private const string PERMISSIONS_KEY = "Permissions";

        /// <summary>
        /// Sets the permissions.
        /// </summary>
        /// <param name="viewData">The view data.</param>
        /// <param name="required">The required.</param>
        private static void SetPermissions(ViewDataDictionary viewData, Permission required)
        {
            viewData[PERMISSIONS_KEY] = required;
        }

        /// <summary>
        /// Gets the permissions.
        /// </summary>
        /// <param name="viewData">The view data.</param>
        /// <returns></returns>
        public static Permission GetPermissions(ViewDataDictionary viewData)
        {
            return (Permission)viewData[PERMISSIONS_KEY];
        }

        /// <summary>
        /// Ensures the user permissions loaded.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        /// <param name="webContext">The web context.</param>
        /// <param name="authorizationService">The authorization service.</param>
        public static void EnsureUserPermissionsLoaded(ControllerContext filterContext, WebContext webContext, IAuthorizationService authorizationService)
        {
            Permission permission = GetUserPermissionsFromAuthorizationService(webContext, authorizationService);

            SetPermissions(filterContext.Controller.ViewData, permission);
        }

        /// <summary>
        /// Populates the users permissions.
        /// </summary>
        /// <param name="webContext">The web context.</param>
        /// <param name="authorizationService">The authorization service.</param>
        /// <returns></returns>
        public static Permission GetUserPermissionsFromAuthorizationService(WebContext webContext, IAuthorizationService authorizationService)
        {
            ProcessFrameworkPrincipal processFrameworkPrincipal = webContext.Principal;
            Permission? permission = processFrameworkPrincipal.ProcessFrameworkIdentity.Permission;
            if (!permission.HasValue)
            {
                permission = authorizationService.GetUserPermissions(processFrameworkPrincipal.Identity.Name);
                processFrameworkPrincipal.ProcessFrameworkIdentity.Permission = permission;
            }
            return permission.Value;
        }
    }
}
