﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ProcessFramework.Client.Common.Helpers
{
    /// <summary>
    /// The responsibility of this class is to provide helper/extension methods for objects.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public static class ObjectHelper
    {
        /// <summary>
        /// Disposes the source object if is disposable.
        /// </summary>
        /// <param name="source">The source.</param>
        public static void DisposeIfIsDisposable(this object source)
        {
            var disposable = source as IDisposable;
            if (disposable != null)
            {
                disposable.Dispose();
            }
        }

        /// <summary>
        /// Makes a deeps copy of an object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">The obj.</param>
        /// <returns></returns>
        public static T DeepCopy<T>(this T obj)
        {
            T result = default(T);
            if (!ReferenceEquals(obj, null))
            {
                using (var ms = new MemoryStream())
                {
                    var formatter = new BinaryFormatter();
                    formatter.Serialize(ms, obj);
                    ms.Position = 0;
                    result = (T) formatter.Deserialize(ms);
                }
            }
            return result;
        }

        /// <summary>
        /// Serializes the given object to the named file.
        /// </summary>
        /// <typeparam name="TObjectToSerialize">The type of the object to serialize.</typeparam>
        /// <param name="objectToSerialize">The object to serialize.</param>
        /// <param name="fileName">Name of the file.</param>
        public static void SerializeTo<TObjectToSerialize>(TObjectToSerialize objectToSerialize, string fileName)
        {
            using (Stream stream = File.OpenWrite(fileName))
            {
                SerializeTo(objectToSerialize, stream);
            }
        }

        /// <summary>
        /// Serializes the given object to the given stream.
        /// </summary>
        /// <typeparam name="TObjectToSerialize">The type of the object to serialize.</typeparam>
        /// <param name="objectToSerialize">The object to serialize.</param>
        /// <param name="stream">The stream.</param>
        public static void SerializeTo<TObjectToSerialize>(TObjectToSerialize objectToSerialize, Stream stream)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, objectToSerialize);
        }

        /// <summary>
        /// Deserializes an object of type <typeparamref name="TReturn"/> from the given stream.
        /// </summary>
        /// <typeparam name="TReturn">The type of the return.</typeparam>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        public static TReturn SerializeFrom<TReturn>(FileStream stream)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            return (TReturn) formatter.Deserialize(stream);
        }
    }
}
