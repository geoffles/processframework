﻿using System;

namespace ProcessFramework.Client.Common.Constants
{
    /// <summary>
    /// The responsibility of this class is to provide constants for app setting keys.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public static class AppSettingsKey
    {

        /// <summary>
        /// DnsHostName.
        /// </summary>
        public static readonly string DnsHostName = "dns.hostname";

        /// <summary>
        /// ApplicationInstanceCountryCode
        /// </summary>
        public static readonly string ApplicationInstanceCountryCode = "application-instance.country-code";

        /// <summary>
        /// ApplicationCredentialsApiKey
        /// </summary>
        public static readonly string ApplicationCredentialsApiKey = "application-credentials.api-key";

        /// <summary>
        /// ApplicationCredentialsToken
        /// </summary>
        public static readonly string ApplicationCredentialsToken = "application-credentials.token";

        /// <summary>
        /// Currencies
        /// </summary>
        public static readonly string Currencies = "currencies";

        /// <summary>
        /// ApplicationInstanceCurrencyIsoCode
        /// </summary>
        public static readonly string ApplicationInstanceCurrencyIsoCode = "application-instance.currency-iso-code";

    }
}
