﻿using System;

namespace ProcessFramework.Client.Common.Constants
{
    /// <summary>
    /// The enumeration of Permission flags.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    [Flags]
    public enum Permission
    {
        /// <summary>
        /// Administrator
        /// </summary>
        Administrator = (1 << 0)
    }
}
