﻿using System;

using Spring.Context;
using Spring.Context.Support;

namespace ProcessFramework.Client.Common.IoC
{
    /// <summary>
    /// The facade for the spring application container.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public class SpringApplicationContextContainer : IContainer
    {
        /// <summary>
        /// Gets the context.
        /// </summary>
        private IApplicationContext Context { get; set; }


        /// <summary>
        /// Determines whether the container contains a mapping for the given object name.
        /// </summary>
        /// <param name="objectName">Name of the object.</param>
        /// <returns>
        ///   <c>true</c> if the container contains a mapping for the given object name; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(string objectName)
        {
            EnsureContext();
            return Context.ContainsObject(objectName);
        }

        /// <summary>
        /// Determines whether the container contains the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        ///   <c>true</c> if the container contains the specified type; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(Type type)
        {
            return Contains(DefaultObjectName(type));
        }

        /// <summary>
        /// Return a instance of an object in the context by the specified name.
        /// </summary>
        /// <param name="objectName">Accepts a string object name.</param>
        public object Resolve(string objectName)
        {
            EnsureContext();
            return Context.GetObject(objectName);
        }

        /// <summary>
        /// Return a instance of an object in the context by the specified name and type.
        /// </summary>
        /// <typeparam name="T">Accepts the type of the object to resolve.</typeparam>
        /// <param name="objectName">Accepts a string object name.</param>
        public T Resolve<T>(string objectName)
        {
            return (T)Resolve(objectName);
        }

        /// <summary>
        /// Resolves the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public object Resolve(Type type)
        {
            EnsureContext();
            return Context.GetObject(DefaultObjectName(type));
        }

        /// <summary>
        /// Ensures the context.
        /// </summary>
        private void EnsureContext()
        {
            if (Context == null)
            {
                Context = ContextRegistry.GetContext();
            }
        }

        /// <summary>
        /// Resolves an instance of type <c>T</c>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Resolve<T>()
        {
            return Resolve<T>(DefaultObjectName(typeof(T)));
        }

        private static string DefaultObjectName(Type type)
        {
            return string.Concat(type.Name, "Object");
        }
    }
}
