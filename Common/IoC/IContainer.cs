﻿using System;

namespace ProcessFramework.Client.Common.IoC
{
    /// <summary>
    /// The contract defining an IoC container
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public interface IContainer
    {
        /// <summary>
        /// Determines whether the container contains a mapping for the given object name.
        /// </summary>
        /// <param name="objectName">Name of the object.</param>
        /// <returns>
        ///   <c>true</c> if the container contains a mapping for the given object name; otherwise, <c>false</c>.
        /// </returns>
        bool Contains(string objectName);

        /// <summary>
        /// Determines whether the container contains the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        ///   <c>true</c> if the container contains the specified type; otherwise, <c>false</c>.
        /// </returns>
        bool Contains(Type type);

        /// <summary>
        /// Return a instance of an object in the context by the specified name.
        /// </summary>
        /// <param name="objectName">Accepts a string object name.</param>
        object Resolve(string objectName);

        /// <summary>
        /// Return a instance of an object in the context by the specified name and type.
        /// </summary>
        /// <typeparam name="T">Accepts the type of the object to resolve.</typeparam>
        /// <param name="objectName">Accepts a string object name.</param>
        T Resolve<T>(string objectName);

        /// <summary>
        /// Resolves the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        object Resolve(Type type);

        /// <summary>
        /// Resolves an instance of type <c>T</c>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T Resolve<T>();
    }
}
