﻿using System;
using System.Configuration;

namespace ProcessFramework.Client.Common.Config
{
    /// <summary>
    /// The responsibility of this class is to represent the user collection under pfx.security/permissionGroups/permissionGroup configuration section.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public sealed class UserCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// Gets the type of the <see cref="T:System.Configuration.ConfigurationElementCollection"/>.
        /// </summary>
        /// <returns>The <see cref="T:System.Configuration.ConfigurationElementCollectionType"/> of this collection.</returns>
        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        /// <summary>
        /// Gets the name used to identify this collection of elements in the configuration file when overridden in a derived class.
        /// </summary>
        /// <returns>
        /// The name of the collection; otherwise, an empty string. The default is an empty string.
        /// </returns>
        protected override string ElementName
        {
            get { return "user"; }
        }

        /// <summary>
        /// Overrides the integer index operator in order to cast the collected types to <see cref="UserElement"/>.
        /// </summary>
        /// <param name="index">The index at which the element should be inserted, or from which is should be retrieved.</param>
        public UserElement this[int index]
        {
            get { return (UserElement) BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        /// <summary>
        /// Overrides the string index operator in order to cast the collected types to <see cref="PermissionGroupElement"/>.
        /// Implicitely models the string key as an user name.
        /// </summary>
        /// <param name="userName">The string index, which is understood to be the user name in this model.</param>
        new public UserElement this[string userName]
        {
            get { return (UserElement) BaseGet(userName); }
        }

        /// <summary>
        /// Creates a new <see cref="UserElement"/>.
        /// </summary>
        /// <returns>
        /// A new <see cref="UserElement"/>.
        /// </returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new UserElement();
        }

        /// <summary>
        /// Gets the element key for a specified configuration element. This is the Name property of the <see cref="UserElement"/>.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Object"/> that acts as the key for the specified <see cref="T:System.Configuration.ConfigurationElement"/>.
        /// </returns>
        /// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement"/> to return the key for. </param>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((UserElement) element).Name;
        }


        /// <summary>
        /// Returns <c>true</c> if the given key is among all the keys maintained by the base class; <c>false</c> otherwise.
        /// </summary>
        /// <param name="key">The key to search for.</param>
        /// <returns><c>true</c> if the given key is among all the keys maintained by the base class; <c>false</c> otherwise.</returns>
        public bool ContainsKey(string key)
        {
            bool result = false;
            object[] keys = BaseGetAllKeys();
            foreach (object obj in keys)
            {
                if ((string) obj == key)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }
    }
}
