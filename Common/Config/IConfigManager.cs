﻿using System;
using System.Collections.Specialized;

namespace ProcessFramework.Client.Common.Config
{
    /// <summary>
    /// The responsibility of this class is to abstract the static <c>ConfigManager</c> class.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public interface IConfigManager
    {
        /// <summary>
        /// Gets the app settings.
        /// </summary>
        NameValueCollection AppSettings { get; }
    }
}