﻿using System;
using System.Configuration;

namespace ProcessFramework.Client.Common.Config
{
    /// <summary>
    /// The responsibility of this class is to represent the permission groups element the under pfx.security configuration section.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public class PermissionGroupCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// Gets the type of the <see cref="T:System.Configuration.ConfigurationElementCollection"/>.
        /// </summary>
        /// <returns>
        /// The <see cref="T:System.Configuration.ConfigurationElementCollectionType"/> of this collection.
        /// </returns>
        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        /// <summary>
        /// Gets the name used to identify this collection of elements in the configuration file when overridden in a derived class.
        /// </summary>
        /// <returns>
        /// The name of the collection; otherwise, an empty string. The default is an empty string.
        /// </returns>
        protected override string ElementName
        {
            get { return "permissionGroup"; }
        }

        /// <summary>
        /// Overrides the integer index operator in order to cast the collected types to <see cref="PermissionGroupElement"/>.
        /// </summary>
        /// <param name="index">The index at which the element should be inserted, or from which is should be retrieved.</param>
        public PermissionGroupElement this[int index]
        {
            get { return (PermissionGroupElement) BaseGet(index); }
            set
            {
                if(BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        /// <summary>
        /// Overrides the string index operator in order to cast the collected types to <see cref="PermissionGroupElement"/>.
        /// Implicitely models the string key as a group name.
        /// </summary>
        /// <param name="groupName">The string index, which is understood to be the group name in this model.</param>
        new public PermissionGroupElement this[string groupName]
        {
            get { return (PermissionGroupElement) BaseGet(groupName); }
        }

        /// <summary>
        /// Creates a new <see cref="PermissionGroupElement"/>.
        /// </summary>
        /// <returns>
        /// A new <see cref="PermissionGroupElement"/>.
        /// </returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new PermissionGroupElement();
        }

        /// <summary>
        /// Gets the element key for a specified configuration element. This is the GroupName property of the <see cref="PermissionGroupElement"/>.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Object"/> that acts as the key for the specified <see cref="T:System.Configuration.ConfigurationElement"/>.
        /// </returns>
        /// <param name="element">The <see cref="T:System.Configuration.ConfigurationElement"/> to return the key for. </param>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((PermissionGroupElement) element).GroupName;
        }

        /// <summary>
        /// Returns <c>true</c> if the given key is among all the keys maintained by the base class; <c>false</c> otherwise.
        /// </summary>
        /// <param name="key">The key to search for.</param>
        /// <returns><c>true</c> if the given key is among all the keys maintained by the base class; <c>false</c> otherwise.</returns>
        public bool ContainsKey(string key)
        {
            bool result = false;
            object[] keys = BaseGetAllKeys();
            foreach(object obj in keys)
            {
                if((string) obj == key)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }
    }
}
