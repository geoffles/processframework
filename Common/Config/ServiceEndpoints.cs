﻿using System;

namespace ProcessFramework.Client.Common.Config
{
    /// <summary>
    /// The responsibility of this class is to hold all service endpoint details.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public class ServiceEndpoints
    {
        
    }
}
