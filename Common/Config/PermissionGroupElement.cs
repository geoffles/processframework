﻿using System;
using System.Configuration;

namespace ProcessFramework.Client.Common.Config
{
    /// <summary>
    /// The responsibility of this class is to represent the permission group element the under pfx.security/permissionGroups configuration section.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public class PermissionGroupElement : ConfigurationElement
    {
        /// <summary>
        /// Gets or sets the name of the group.
        /// </summary>
        /// <value>
        /// The name of the group.
        /// </value>
        [ConfigurationProperty("groupName", IsRequired = true)]
        public string GroupName
        {
            get { return this["groupName"] as string; }
            set { this["groupName"] = value; }
        }

        /// <summary>
        /// Gets the users.
        /// </summary>
        [ConfigurationProperty("", IsDefaultCollection = true)]
        public UserCollection Users
        {
            get { return (UserCollection) base[""]; }
        }
    }
}
