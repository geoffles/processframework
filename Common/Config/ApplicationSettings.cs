﻿using System;

using ProcessFramework.Client.Common.Constants;


namespace ProcessFramework.Client.Common.Config
{
    /// <summary>
    /// The responsibility of this class is to represent the settings for the application instance.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public class ApplicationSettings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationSettings"/> class.
        /// </summary>
        public ApplicationSettings()
        {
            ServiceEndpoints = new ServiceEndpoints();
            Credentials = new ApplicationCredentials();
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationSettings"/> class.
        /// </summary>
        /// <param name="configManager">The config manager.</param>
        public ApplicationSettings(IConfigManager configManager) : this()
        {
            if (configManager == null)
            {
                throw new ArgumentNullException("configManager");
            }
            ConfigManager = configManager;
            Initialize();
        }

        private IConfigManager ConfigManager { get; set; }

        /// <summary>
        /// Gets or sets the country code.
        /// </summary>
        /// <value>
        /// The country code.
        /// </value>
        public string CountryCode { get; set; }

        /// <summary>
        /// Gets the service endpoints.
        /// </summary>
        public ServiceEndpoints ServiceEndpoints { get; private set; }

        /// <summary>
        /// Gets the credentials.
        /// </summary>
        public ApplicationCredentials Credentials { get; private set; }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        private void Initialize()
        {
            Credentials.APIKey = ConfigManager.AppSettings[AppSettingsKey.ApplicationCredentialsApiKey];
            Credentials.Token = ConfigManager.AppSettings[AppSettingsKey.ApplicationCredentialsToken];
            CountryCode = ConfigManager.AppSettings[AppSettingsKey.ApplicationInstanceCountryCode];
        }
    }
}
