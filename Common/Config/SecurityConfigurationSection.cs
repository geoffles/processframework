﻿using System;
using System.Configuration;

namespace ProcessFramework.Client.Common.Config
{
    /// <summary>
    /// The responsibility of this class is to handle the permission groups configuration section.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public class SecurityConfigurationSection : ConfigurationSection
    {
        private static SecurityConfigurationSection _security;

        /// <summary>
        /// Gets the security configuration section.
        /// </summary>
        public static SecurityConfigurationSection Security
        {
            get { return _security ?? (_security = GetPermissionGroupsConfigurationSection()); }
        }

        /// <summary>
        /// Gets the permission groups.
        /// </summary>
        [ConfigurationProperty("permissionGroups", IsDefaultCollection = true)]
        public PermissionGroupCollection PermissionGroups
        {
            get { return (PermissionGroupCollection) base["permissionGroups"]; }
        }

        private static SecurityConfigurationSection GetPermissionGroupsConfigurationSection()
        {
            object section = ConfigurationManager.GetSection("pfx.security");
            var result = section as SecurityConfigurationSection;
            return result;
        }
    }
}
