﻿using System;
using System.Collections.Specialized;
using System.Configuration;

namespace ProcessFramework.Client.Common.Config
{
    /// <summary>
    /// The responsibility of this class is to provide a layer over the default configuration manager.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public class ConfigManager : IConfigManager
    {
        #region IConfigManager Members

        /// <summary>
        /// Gets the app settings.
        /// </summary>
        public NameValueCollection AppSettings
        {
            get { return ConfigurationManager.AppSettings; }
        }

        #endregion
    }
}
