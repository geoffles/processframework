﻿using System;
using System.Runtime.Caching;

using ProcessFramework.Client.Common.Helpers;

namespace ProcessFramework.Client.Common.Cache
{
    /// <summary>
    /// The responsibility of this class is to provide a value caching coordinator. Values are retrieved by providing an association key.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public class DefaultCacheCoordinator : ICacheCoordinator, IDisposable
    {
        private static readonly TimeSpan _defaultCacheTimeoutInterval = TimeSpan.FromMinutes(5);
        private bool _isDisposed;
        private MemoryCache _memoryCache;

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultCacheCoordinator"/> class.
        /// </summary>
        /// <param name="cacheTimeoutInterval">The cache timeout interval.</param>
        public DefaultCacheCoordinator(TimeSpan? cacheTimeoutInterval = null)
        {
            CacheTimeoutInterval = cacheTimeoutInterval ?? _defaultCacheTimeoutInterval;
        }

        private MemoryCache MemoryCache
        {
            get { return _memoryCache ?? (_memoryCache = new MemoryCache(Guid.NewGuid().ToString())); }
        }

        TimeSpan CacheTimeoutInterval { get; set; }

        #region ICacheCoordinator Members

        /// <summary>
        /// Gets the <see cref="System.Object"/> with the specified key.
        /// </summary>
        public object this[string key]
        {
            get { return MemoryCache[key]; }
            set { Set(key, value); }
        }

        /// <summary>
        /// Determines whether the specified key exists or not.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        ///   <c>true</c> if [contains] [the specified key]; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(string key)
        {
            return MemoryCache.Contains(key);
        }

        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        private void Set(string key, object value)
        {
            CacheItem cacheItem = new CacheItem(key, value);
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy
                {
                    SlidingExpiration = CacheTimeoutInterval,
                    RemovedCallback = OnRemoveCachedItem
                };

            MemoryCache.Set(cacheItem, cacheItemPolicy);
        }

        private static void OnRemoveCachedItem(CacheEntryRemovedArguments args)
        {
            var cacheItem = args.CacheItem;
            cacheItem.DisposeIfIsDisposable();
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        ~DefaultCacheCoordinator()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    ReleaseManagedResources();
                }
                ReleaseUnmanagedResources();
                _isDisposed = true;
            }
        }

        protected virtual void ReleaseUnmanagedResources()
        {}

        protected virtual void ReleaseManagedResources()
        {
            MemoryCache.Dispose();
        }
    }

    /// <summary>
    /// The generic implementation of <c>DefaultCacheCoordinator</c>.
    /// <remarks>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-02-28<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </remarks>
    /// </summary>
    public class DefaultCacheCoordinator<T>
    {
        private readonly DefaultCacheCoordinator _defaultCacheCoordinator;

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultCacheCoordinator&lt;T&gt;"/> class.
        /// </summary>
        /// <param name="cacheTimeoutInterval">The cache timeout interval.</param>
        public DefaultCacheCoordinator(TimeSpan? cacheTimeoutInterval = default(TimeSpan?))
        {
            _defaultCacheCoordinator = new DefaultCacheCoordinator(cacheTimeoutInterval);
        }

        private DefaultCacheCoordinator CacheCoordinator
        {
            get { return _defaultCacheCoordinator; }
        }

        /// <summary>
        /// Gets the <see cref="T"/> with the specified key.
        /// </summary>
        public T this[string key]
        {
            get { return (T) CacheCoordinator[key]; }
            set { CacheCoordinator[key] = value; }
        }

        /// <summary>
        /// Determines whether the specified key exists or not.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        ///   <c>true</c> if [contains] [the specified key]; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(string key)
        {
            return CacheCoordinator.Contains(key);
        }
    }
}
