﻿using System;

using ProcessFramework.Client.Common.Constants;

namespace ProcessFramework.Client.Common.Cache
{
    /// <summary>
    /// The interface defining a cache coordinator.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-06<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public interface ICacheCoordinator
    {
        /// <summary>
        /// Gets the <see cref="System.Object"/> with the specified key.
        /// </summary>
        object this[string key] { get; set; }

        /// <summary>
        /// Determines whether [contains] [the specified key].
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        ///   <c>true</c> if [contains] [the specified key]; otherwise, <c>false</c>.
        /// </returns>
        bool Contains(string key);
    }
}
