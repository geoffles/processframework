using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace ProcessFramework.Client
{
    /// <summary>
    /// The responsibility of this class is to detect client refresh actions and allow any verb for the requested action.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-09-17<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public sealed class RefreshingActionSelector : ApiControllerActionSelector
    {
        /// <summary>
        /// If "Fetch-Only" is defined in header, selects action based on name. Otherwise uses default implementation.
        /// </summary>
        public override HttpActionDescriptor SelectAction(HttpControllerContext controllerContext)
        {
            if (controllerContext.Request.Headers.Any(p => p.Key.Equals("Fetch-Only")))
            {
                object targetAction;
                controllerContext.RouteData.Values.TryGetValue("action", out targetAction);

                var methods = controllerContext
                    .ControllerDescriptor
                    .ControllerType
                    .GetMethods(BindingFlags.Instance | BindingFlags.Public)
                    .Where(IsValidAction)
                    .Where(p => p.Name == targetAction.ToString())
                    .ToList();

                if (methods.Count  == 0)
                {
                    throw new HttpResponseException(controllerContext.Request.CreateErrorResponse(HttpStatusCode.NotFound, "Resource not found"));
                }
                else if (methods.Count > 1)
                {
                    throw new HttpResponseException(controllerContext.Request.CreateErrorResponse(HttpStatusCode.NotFound, "Ambiguous Reference"));
                }

                return new ReflectedHttpActionDescriptor(controllerContext.ControllerDescriptor, methods.First());
            }

            return base.SelectAction(controllerContext);
        }

        private static bool IsValidAction(MethodInfo methodInfo)
        {
            return !methodInfo.IsSpecialName &&
                   !methodInfo.GetBaseDefinition().DeclaringType.IsAssignableFrom(typeof(ApiController)) &&
                   !methodInfo.GetCustomAttributes(typeof(NonActionAttribute), true).Any();
        }
    }
}
