using System.Web.Http.WebHost;
using System.Web.Routing;
using System.Web.SessionState;

namespace ProcessFramework.Client
{
    /// <summary>
    /// The responsibility of this class is enable sessioning in WebAPI calls
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-06-24<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    //http://forums.asp.net/t/1780385.aspx/1
    public class SessionHttpControllerHandler : HttpControllerHandler, IRequiresSessionState
    {
        public SessionHttpControllerHandler(RouteData routeData)
            : base(routeData)
        {
        }
    }
}