using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

using ProcessFramework.Client.Helpers;
using ProcessFramework.Client.Models;
using ProcessFramework.Client.WebApiExtensions;

using Newtonsoft.Json;

namespace ProcessFramework.Client
{
    /// <summary>
    /// The responsibility of this class is to provide process transition response messages as JSON.
    /// This class wraps a JSON serializer. The serialized object is wrapped in a TabModel container prior to JSON serialization.
    /// 
    /// An instance of this class should be added to the HttpConfiguration Formatters collection at startup.
    /// 
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-07-19<br/>
    /// <para/>
    /// Modified By : Another Dev (anon for privacy)<br/>
    /// Date        : 2014-01-16<br/>
    /// Details     : Moved tab model construction code to its own helper class.<br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public class ProcessMetaDataJsonMediaTypeFormatter : MediaTypeFormatter
    {
        private JsonMediaTypeFormatter _jsonMediaTypeFormatter;

        /// <summary>
        /// Create a new Instance.
        /// </summary>
        public ProcessMetaDataJsonMediaTypeFormatter()
        {
            _jsonMediaTypeFormatter = new JsonMediaTypeFormatter();
            _jsonMediaTypeFormatter.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;

            SupportedMediaTypes.Add(new MediaTypeHeaderValue(Constants.PROCESS_MIME_TYPE));
        }

        /// <summary>
        /// Wraps <c>value</c> in the process meta data and then writes that as JSON serialised data to the stream.
        /// </summary>
        public override Task WriteToStreamAsync(Type type, object value, Stream writeStream, HttpContent content, TransportContext transportContext)
        {
            if (value is HttpError)
            {
                return _jsonMediaTypeFormatter.WriteToStreamAsync(type, value, writeStream, content, transportContext);
            }

            TabModel tabModel = TabModelHelper.BuildTabModel(value);

            return _jsonMediaTypeFormatter.WriteToStreamAsync(type, tabModel, writeStream, content, transportContext);
        }

        /// <summary>
        /// Queries whether this <see cref="T:System.Net.Http.Formatting.MediaTypeFormatter"/> can deserializean object of the specified type.
        /// </summary>
        /// <returns>
        /// true if the <see cref="T:System.Net.Http.Formatting.MediaTypeFormatter"/> can deserialize the type; otherwise, false.
        /// </returns>
        /// <param name="type">The type to deserialize.</param>
        public override bool CanReadType(Type type)
        {
            if (typeof(HttpError).IsAssignableFrom(type))
            {
                return false;
            }
            return _jsonMediaTypeFormatter.CanReadType(type);
        }

        /// <summary>
        /// Queries whether this <see cref="T:System.Net.Http.Formatting.MediaTypeFormatter"/> can serializean object of the specified type.
        /// </summary>
        /// <returns>
        /// true if the <see cref="T:System.Net.Http.Formatting.MediaTypeFormatter"/> can serialize the type; otherwise, false.
        /// </returns>
        /// <param name="type">The type to serialize.</param>
        public override bool CanWriteType(Type type)
        {
            if (typeof(HttpError).IsAssignableFrom(type))
            {
                return false;
            }
            return _jsonMediaTypeFormatter.CanWriteType(type);
        }
    }
}
