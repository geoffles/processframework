using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

using ProcessFramework.Client.Common.Validation;
using ProcessFramework.Client.Helpers;
using ProcessFramework.Client.Models;

using Newtonsoft.Json;

namespace ProcessFramework.Client.WebApiExtensions
{
    /// <summary>
    /// The responsibility of this class is to set the content type to text/html for JSON errors. 
    /// This addresses the issue of IE 9 trying to present JSON responses as a file download.
    /// 
    /// An instance of this class should be added to the HttpConfiguration Formatters collection at startup.
    /// 
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-07-19<br/>
    /// <para/>
    /// Modified By : Another Dev (anon for privacy)<br/>
    /// Date        : 2014-01-16<br/>
    /// Details     : Added special handling of validation failure exceptions.<br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public class IeJsonErrorMediaTypeFormatter : MediaTypeFormatter
    {
        private JsonMediaTypeFormatter _jsonMediaTypeFormatter;

        /// <summary>
        /// Create a new Instance.
        /// </summary>
        public IeJsonErrorMediaTypeFormatter()
        {
            _jsonMediaTypeFormatter = new JsonMediaTypeFormatter();
            _jsonMediaTypeFormatter.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;

            SupportedMediaTypes.Add(new MediaTypeHeaderValue(Constants.TEXT_HTML_MIME_TYPE));
            SupportedMediaTypes.Add(new MediaTypeHeaderValue(Constants.APPLICATION_JSON));
        }

        /// <summary>
        /// Wraps <c>value</c> in the process meta data and then writes that as JSON serialised data to the stream.
        /// </summary>
        public override Task WriteToStreamAsync(Type type, object value, Stream writeStream, HttpContent content, TransportContext transportContext)
        {
            HttpError httpError = value as HttpError;
            if (httpError != null)
            {
                if (httpError.ContainsKey("ExceptionType") && httpError["ExceptionType"].Equals(typeof(ValidationFailedException).FullName))
                {
                    TabModel tabModel = TabModelHelper.BuildTabModel(null, true);
                    value = tabModel;
                    type = value.GetType();
                    content = new ObjectContent<TabModel>(tabModel, _jsonMediaTypeFormatter);
                }
            }
            return _jsonMediaTypeFormatter.WriteToStreamAsync(type, value, writeStream, content, transportContext);
        }

        /// <summary>
        /// Queries whether this <see cref="T:System.Net.Http.Formatting.MediaTypeFormatter"/> can deserializean object of the specified type.
        /// </summary>
        /// <returns>
        /// true if the <see cref="T:System.Net.Http.Formatting.MediaTypeFormatter"/> can deserialize the type; otherwise, false.
        /// </returns>
        /// <param name="type">The type to deserialize.</param>
        public override bool CanReadType(Type type)
        {
            return false;
        }

        /// <summary>
        /// Queries whether this <see cref="T:System.Net.Http.Formatting.MediaTypeFormatter"/> can serializean object of the specified type.
        /// </summary>
        /// <returns>
        /// true if the <see cref="T:System.Net.Http.Formatting.MediaTypeFormatter"/> can serialize the type; otherwise, false.
        /// </returns>
        /// <param name="type">The type to serialize.</param>
        public override bool CanWriteType(Type type)
        {
            return typeof(HttpError).IsAssignableFrom(type);
        }
    }
}
