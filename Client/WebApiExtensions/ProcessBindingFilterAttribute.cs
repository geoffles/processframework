﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using ProcessFramework.Client.Common.Process;
using ProcessFramework.Client.Common.Validation;

namespace ProcessFramework.Client
{
    /// <summary>
    /// The responsibility of this class is to perform binding of the process to the http request.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-09-17<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public class ProcessBindingFilterAttribute : ActionFilterAttribute 
    {
        /// <summary>
        /// Checks for completed processes and removes them from the session.
        /// </summary>
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (ProcessManager.Instance.WebActionProcess.CurrentState is ProcessCompletedResult)
            {
                ProcessManager.Instance.RemoveProcess(ProcessManager.Instance.WebActionProcess.TabId);
            }
        }

        /// <summary>
        /// Performs the binding of validation and process managers to the request.
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            bool isTabRefresh = CheckForRefresh(actionContext);
            Guid tabId = isTabRefresh ? GetTabId(actionContext) : CheckInitialiseAndGetTabId(actionContext);
            
            ProcessManager.Instance.Attach(tabId, isTabRefresh);
            ValidationManager.Instance.Attach(ProcessManager.Instance.WebActionProcess); 
            
            base.OnActionExecuting(actionContext);
        }

        private bool CheckForRefresh(HttpActionContext actionContext)
        {
            IEnumerable<string> headerValues;
            return actionContext.Request.Headers.TryGetValues("Fetch-Only", out headerValues);
        }

        private Guid CheckInitialiseAndGetTabId(HttpActionContext actionContext)
        {
            string actionName = actionContext.ActionDescriptor.ActionName;
            Type controllerType = actionContext.ControllerContext.ControllerDescriptor.ControllerType;

            MethodBase action = controllerType
                .GetMethods()
                .Where(method => method.Name == actionName)
                .FirstOrDefault(method => !method.GetCustomAttributes(typeof (System.Web.Http.NonActionAttribute), true).Any());
            
            if (action == null)
            {
                throw new Exception("Error finding action to invoke");
            }

            ProcessInitialiserAttribute initialiserAttribute =
                action.GetCustomAttributes(typeof (ProcessInitialiserAttribute), true)
                    .OfType<ProcessInitialiserAttribute>()
                    .FirstOrDefault();

            if (initialiserAttribute != null)
            {
                return ProcessManager.Instance.InitialiseProcess(controllerType, action);
            }
            else
            {
                return GetTabId(actionContext);
            }
        }

        private static Guid GetTabId(HttpActionContext actionContext)
        {
            NameValueCollection queryParams = actionContext.Request.RequestUri.ParseQueryString();
            string queryTabIdString = queryParams["TabId"];

            if ( !string.IsNullOrEmpty(queryTabIdString) )
            {
                Guid tabId;
                if (!Guid.TryParse(queryTabIdString, out tabId))
                {
                    throw new Exception("The TabId supplied in the query string cannot be parsed as a GUID");
                }
                return tabId;
            }
            else
            {
                return GetTabIdFromHeader(actionContext);
            }
        }

        private static Guid GetTabIdFromHeader(HttpActionContext actionContext)
        {
            IEnumerable<string> tabIds;
            if (!actionContext.Request.Headers.TryGetValues("Tab-Id", out tabIds))
            {
                throw new Exception("No Tab-Id header field is defined in HTTP request");
            }

            string tabIdStr = tabIds.FirstOrDefault();
            if (string.IsNullOrEmpty(tabIdStr))
            {
                throw new Exception("No Tab-Id value defined in HTTP request header");
            }

            Guid tabId;
            if (!Guid.TryParse(tabIdStr, out tabId))
            {
                throw new Exception("The Tab-Id value is not a GUID.");
            }
            return tabId;
        }
    }
}