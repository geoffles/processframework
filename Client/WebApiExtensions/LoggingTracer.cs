using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http.Tracing;
using log4net;
using System.Linq;
using log4net.Config;
using TraceLevel = System.Web.Http.Tracing.TraceLevel;

namespace ProcessFramework.Client.WebApiExtensions
{
    /// <summary>
    /// The responsibility of this class is to ... <some diligent developer to fill this in.>
    /// It collaborates with ... <same diligent developer to fill this in>
    /// <remarks>
    /// Created By  : firstname lastname<br/>
    /// Date        : 2013-mm-dd<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public sealed class LoggingTracer : ITraceWriter
    {
        private ILog Log;

        public LoggingTracer()
        {
            var httpContext = HttpContext.Current;
            if (httpContext != null)
            {
                string l4Net = httpContext.Server.MapPath("~/Config/log4net.config");
                XmlConfigurator.ConfigureAndWatch(new FileInfo(l4Net));
                Log = LogManager.GetLogger("ProcessFrameworkClient");
            }
        }
        
        /// <summary>
        /// Invokes the specified traceAction to allow setting values in a new <see cref="T:System.Web.Http.Tracing.TraceRecord"/> if and only if tracing is permitted at the given category and level. 
        /// </summary>
        /// <param name="request">The current <see cref="T:System.Net.Http.HttpRequestMessage"/>.   It may be null but doing so will prevent subsequent trace analysis  from correlating the trace to a particular request.</param><param name="category">The logical category for the trace.  Users can define their own.</param><param name="level">The <see cref="T:System.Web.Http.Tracing.TraceLevel"/> at which to write this trace.</param><param name="traceAction">The action to invoke if tracing is enabled.  The caller is expected to fill in the fields of the given <see cref="T:System.Web.Http.Tracing.TraceRecord"/> in this action.</param>
        public void Trace(HttpRequestMessage request, string category, TraceLevel level, Action<TraceRecord> traceAction)
        {
            TraceRecord record = new TraceRecord(request, category, level);
            traceAction(record);
            LogTraceToLog4Net(record);
        }

        private void LogTraceToLog4Net(TraceRecord record)
        {
            if (Log == null || record == null)
            {
                return;
            }

            switch(record.Level)
            {
                case TraceLevel.Off:
                    break;
                case TraceLevel.Debug:
                    Log.Debug(GetMessage(record));
                    break;
                case TraceLevel.Info:
                    Log.Info(GetMessage(record));
                    break;
                case TraceLevel.Warn:
                    Log.Warn(GetMessage(record));
                    break;
                case TraceLevel.Error:
                    Log.Error(GetMessage(record));
                    break;
                case TraceLevel.Fatal:
                    Log.Fatal(GetMessage(record));
                    break;
                default:
                    Log.Warn("Unknown Trace Level. Message: " + GetMessage(record));
                    break;
            }
        }

        private string GetMessage(TraceRecord record)
        {
            return string.Format("[{7}({4}):{0}-{1}-{2}]\t{3}\t{5}\t{6}",
                                 record.Category,
                                 record.Operation,
                                 record.Operator,
                                 record.Status,
                                 record.RequestId,
                                 (record.Request != null && record.Request.RequestUri != null) ? record.Request.RequestUri.ToString() : string.Empty,
                                 record.Exception != null ? record.Exception.ToString() : string.Empty,
                                 record.Kind
                                 );
        }
    }
}
