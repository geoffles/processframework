using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessFramework.Client.WebApiExtensions
{
    /// <summary>
    /// The responsibility of this class is to contain mime types used by the system
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-mm-dd<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public sealed class Constants
    {
        /// <summary>
        /// Mime type for process view data
        /// </summary>
        public const string PROCESS_MIME_TYPE = "application/vnd.process.json";

        /// <summary>
        /// Text HTML used for IE error compatibility.
        /// </summary>
        public const string TEXT_HTML_MIME_TYPE = "text/html";

        /// <summary>
        /// Application JSON used for HTTP errors.
        /// </summary>
        public const string APPLICATION_JSON = "application/json";
    }
}
