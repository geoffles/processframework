﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace ProcessFramework.Client.App_Start
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //API Requests
            var route = routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{area}/{controller}/{action}",
                defaults: new { model = RouteParameter.Optional }
            );
            //Allow API route to use the session.
            route.RouteHandler = new SessionHttpControllerRouteHandler();

            //View Requests
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}