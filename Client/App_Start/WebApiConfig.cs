﻿using System;
using System.ComponentModel;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Tracing;

using ProcessFramework.Client.WebApiExtensions;
using Newtonsoft.Json;

using BooleanConverter = ProcessFramework.Client.Common.TypeConverters.BooleanConverter;

namespace ProcessFramework.Client.App_Start
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //Web api routes defined in 'RouteConfig.cs'.
            config.Formatters.JsonFormatter.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Local;
            config.Formatters.Insert(0, new IeJsonErrorMediaTypeFormatter());
            config.Formatters.Insert(0, new ProcessMetaDataJsonMediaTypeFormatter());
            

            config.Services.Replace(typeof(IHttpActionSelector), new RefreshingActionSelector());
            config.Services.Replace(typeof(ITraceWriter), new LoggingTracer());

            TypeDescriptor.AddAttributes(typeof(Boolean), new TypeConverterAttribute(typeof(BooleanConverter)));
        }
    }
}
