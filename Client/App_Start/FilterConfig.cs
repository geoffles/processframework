﻿using System;
using System.Web.Mvc;

using ProcessFramework.Client.Common.Attributes;
using ProcessFramework.Client.Logging;

namespace ProcessFramework.Client.App_Start
{
    /// <summary>
    /// Static helper class for providing Global Filter Configuration.
    /// <remarks>
    /// <para>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-??<br/>
    /// </para>
    /// <para>
    /// Modified By : Another Dev (anon for privacy)<br/>
    /// Date        : 2013-08-13<br/>
    /// Details     : Added PopulateUserPermissionsFilterAttribute.<br/>
    /// </para>
    /// </remarks>
    /// </summary>
    public static class FilterConfig
    {
        /// <summary>
        /// Registers the global filters used by the ProcessFramework client.
        /// </summary>
        /// <param name="filters">The global filter collection to which the additional filters should be added.</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new LoggingHandleErrorAttribute());
            filters.Add(new PopulateUserPermissionsFilterAttribute());
        }
    }
}
