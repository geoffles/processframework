﻿using System.Web.Optimization;

namespace ProcessFramework.Client.App_Start
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js", "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                        "~/Scripts/knockout-{version}.js", "~/Scripts/json2.js"));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                        "~/Scripts/kendo.web.js", "~/Scripts/kendo-ko-binding.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/moment").Include(
                        "~/Scripts/moment.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/fx")
                .Include("~/scripts/date-helpers.js")
                .Include("~/scripts/mvc-helpers.js")
                .Include("~/scripts/Fx/Fx.js")
                .Include("~/scripts/Fx/ApplicationContext.js")
                .Include("~/scripts/Fx/ViewController.js")
                .Include("~/scripts/Fx/TabController.js")
                .Include("~/scripts/Fx/ProcessController.js")
                .Include("~/scripts/Fx/ValidationController.js")
                .Include("~/scripts/Fx/grid-common.js")
                .Include("~/scripts/Fx/validation.js"));


            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css", "~/Content/fluid-grid.css", "~/Content/reset.css", "~/Content/errorBanner.css", "~/Content/grid-style.css"));
            bundles.Add(new StyleBundle("~/Content/kendo").Include("~/Content/kendo.common.css", "~/Content/kendo.default.css", "~/Content/kendo-pfx.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
        }
    }
}