﻿Template has been modified to:
 - Add Constants for the controller area.
 - Add API Controllers to the "metadata".

Added:
@:249
        [<#= GeneratedCode #>]
        public const string AreaConst = "<#=ProcessAreaOrControllerName(controller.AreaName) #>";
