using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using log4net;
using log4net.Config;
using log4net.Core;

namespace ProcessFramework.Client.Logging
{
    /// <summary>
    /// The responsibility of this class is to ... <some diligent developer to fill this in.>
    /// It collaborates with ... <same diligent developer to fill this in>
    /// <remarks>
    /// Created By  : firstname lastname<br/>
    /// Date        : 2013-mm-dd<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public sealed class LoggingHandleErrorAttribute : HandleErrorAttribute
    {
        private ILog _log;
        private ILog Log
        {
            get { return _log ?? (_log = CreateLog()); }
        }

        private ILog CreateLog()
        {
            var httpContext = HttpContext.Current;
            if (httpContext != null)
            {
                string l4Net = httpContext.Server.MapPath("~/Config/log4net.config");
                XmlConfigurator.ConfigureAndWatch(new FileInfo(l4Net));
                return LogManager.GetLogger("ProcessFrameworkClient");
            }

            return null;
        }

        public override void OnException(ExceptionContext filterContext)
        {
            if (Log != null)
            {
                Log.Error(string.Format("MVC Exception: {0}", filterContext.Exception));
            }
        }
    }
}
