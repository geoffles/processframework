﻿$OnViewReady(function (model) {

    var viewModel = {
        GoToB: ko.observable(model.GoToB),
        SomeTextValue: ko.observable(model.SomeTextValue),
        toPostModel: function () {
            return ko.toJSON($ViewContext().viewModel);
        }
    };

    $ViewContext().viewModel = viewModel;

    $ApplyViewBindings($ViewContext().viewModel);
})