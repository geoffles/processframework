﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessFramework.Client.Areas.Foo.Models
{
    [Serializable]
    public class FooProcessModel
    {
        public bool GoToB { get; set; }
        public string SomeText { get; set; }

        public System.IO.MemoryStream ContentStream { get; set; }
    }
}
