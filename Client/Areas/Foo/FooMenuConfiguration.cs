﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProcessFramework.Client.Areas.Foo.Processes.Foo;
using ProcessFramework.Client.Common.Process.Menu;

namespace ProcessFramework.Client.Areas.Foo
{
    /// <summary>
    /// The Foo section is for demo purposes and should only be included when the INCLUDE_FOO directive is added
    /// </summary>
    [MenuConfiguration]
    public class FooMenuConfiguration : IMenuConfiguration
    {
        /// <summary>
        /// Gets the process groups for the LHS menu groups
        /// </summary>
        public IList<ProcessGroup> GetMenuGroups()
        {
#if (INCLUDE_FOO)
            return new List<ProcessGroup>
                       {
                           new ProcessGroup
                               {
                                   GroupKey = "Foo",
                                   Label = "Foo Tests"
                               }
                       };
#else
            return new List<ProcessGroup>();
#endif

        }

        /// <summary>
        /// Gets the processes for the LHS menu items
        /// </summary>
        public IList<ProcessGrouping> GetMenuItems()
        {
#if (INCLUDE_FOO)
            return new List<ProcessGrouping>
                       {
                           new ProcessGrouping
                               {
                                   GroupKey = "Foo",
                                   Label = "Foozample",
                                   Order = 1,
                                   Process = typeof(FooProcess)
                               }
                       };
#else
            return new List<ProcessGrouping>();
#endif
        }
    }
}