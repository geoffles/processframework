﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProcessFramework.Client.Common.Process;

namespace ProcessFramework.Client.Areas.Foo.Controllers
{
    public partial class FooController : Controller
    {
        public virtual ActionResult Index()
        {
            ProcessMetaModel model = new ProcessManager().GetMetaModel();
            return View(model);
        }

        public virtual ActionResult Step2A()
        {
            ProcessMetaModel model = new ProcessManager().GetMetaModel();
            return View(model);
        }

        public virtual ActionResult Step2B()
        {
            ProcessMetaModel model = new ProcessManager().GetMetaModel();
            return View(model);
        }

        public virtual ActionResult Step3()
        {
            ProcessMetaModel model = new ProcessManager().GetMetaModel();
            return View(model);
        }
    }
}
