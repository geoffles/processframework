﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ProcessFramework.Client.Areas.Foo.Models;
using ProcessFramework.Client.Common.Process;

namespace ProcessFramework.Client.Areas.Foo.Controllers
{
    [ProcessBindingFilter]
    public partial class FooApiController : ApiController
    {
        [HttpGet]
        [ProcessInitialiser]
        public virtual IndexViewModel Index()
        {
            var processModel = ProcessManager.Instance.ProcessModel<FooProcessModel>();

            return new IndexViewModel
            {
                GoToB = processModel.GoToB,
                SomeTextValue = processModel.SomeText
            };
        }

        [HttpPost]
        public virtual object DoAServerSideBranch([FromBody] IndexViewModel model)
        {
            var processModel = ProcessManager.Instance.Transition<FooProcessModel>(model);
            
            return new { SomeFoo="Howdy", SomeBar=string.Format("You said: {0}", processModel.SomeText)};
        }

        [HttpPost]
        public virtual object BackToIndex()
        {
            var processModel = ProcessManager.Instance.Transition<FooProcessModel>();

            return new IndexViewModel
            {
                GoToB = processModel.GoToB,
                SomeTextValue = processModel.SomeText
            };
        }

        [HttpPost]
        public virtual object Back()
        {
            var processModel = ProcessManager.Instance.Transition<FooProcessModel>();

            return new { SomeFoo = "Howdy", SomeBar = string.Format("You said: {0}", processModel.SomeText) };
        }

        [HttpPost]
        public virtual object Next()
        {
            var processModel = ProcessManager.Instance.Transition<FooProcessModel>();

            return new { SomeFoo = "Howdy", SomeBar = string.Format("You said: {0}", processModel.SomeText) };
        }

        [HttpPost]
        public virtual object Exit()
        {
            ProcessManager.Instance.Transition<FooProcessModel>();
            return null;
        }

        public virtual HttpResponseMessage PostFileDownload()
        {
            var processModel = ProcessManager.Instance.Transition<FooProcessModel>();

            string filename = string.Format("YourFile_{0}.txt", DateTime.Now.ToString("yyyyMMdd_HHmmss"));

            HttpResponseMessage response = FileDownloadHelper.CreateFileDownloadResponseMessage(processModel.ContentStream, filename);

            return response;
        }
    }
}
