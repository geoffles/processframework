﻿using System;

namespace ProcessFramework.Client.Areas.Foo.Processes.Foo
{
    public partial class FooProcess
    {
        [Serializable]
        public class Step2A : Step2Base, IStep2A
        {
            public IStep3 CarryOn()
            {
                return new Step3();
            }

            public IStep1 Back()
            {
                return new Step1();
            }
        }
    }
}