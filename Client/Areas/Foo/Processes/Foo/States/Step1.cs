﻿using System;
using ProcessFramework.Client.Areas.Foo.Models;
using ProcessFramework.Client.Common.Process;

namespace ProcessFramework.Client.Areas.Foo.Processes.Foo
{
    public partial class FooProcess
    {
        [Serializable]
        public class Step1 : ProcessStateBase<FooProcessModel>, IStep1
        {
            public Step2Base Branch(IndexViewModel viewModel)
            {
                Model.GoToB = viewModel.GoToB;
                Model.SomeText = viewModel.SomeTextValue;
                return Model.GoToB ? (Step2Base)new Step2B() : new Step2A();
            }
        }
    }
}