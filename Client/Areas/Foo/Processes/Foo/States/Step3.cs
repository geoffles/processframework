﻿using System;
using ProcessFramework.Client.Areas.Foo.Models;
using ProcessFramework.Client.Common.Process;
using System.IO;
using System.Web.Mvc;

namespace ProcessFramework.Client.Areas.Foo.Processes.Foo
{
    public partial class FooProcess
    {
        [Serializable]
        public class Step3 : ProcessStateBase<FooProcessModel>, IStep3
        {
            public IStep3 ProduceDownload()
            {
                var ms = new MemoryStream();
                var sr = new StreamWriter(ms);
                
                sr.WriteLine("Did you choose go straight to 2B?:{0}", Model.GoToB ? "Yes" : "No");
                sr.WriteLine(Model.SomeText);
                
                sr.Flush();

                ms.Seek(0, SeekOrigin.Begin);

                Model.ContentStream = ms;
                
                return this;
            }

            public ProcessCompletedResult EndWithClose()
            {
                return new ProcessCompletedResult();
            }

            public IProcessState Back()
            {
                return Model.GoToB ? (IProcessState)new Step2B() : new Step2A();
            }
        }
    }
}