﻿using ProcessFramework.Client.Common.Process;
using System;
using System.IO;
using System.Web.Mvc;

namespace ProcessFramework.Client.Areas.Foo.Processes.Foo
{
    public partial class FooProcess
    {
        [Serializable]
        public class Step2B : Step2Base, IStep2B
        {
            public IStep3 NextPlease()
            {
                return new Step3();
            }
            
            public IStep2A Back()
            {
                return new Step2A();
            }

            public WrapAroundProcessCompletedResult EndWithNew()
            {
                return new WrapAroundProcessCompletedResult();
            }
        }
    }
}