﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProcessFramework.Client.Areas.Foo.Models;
using ProcessFramework.Client.Common.Process;

namespace ProcessFramework.Client.Areas.Foo.Processes.Foo
{
    public partial class FooProcess : ProcessBase<FooProcessModel>
    {
        protected override void OnInitialise()
        {
            CurrentState = new Step1();
            TypedModel = new FooProcessModel { GoToB = true };
        }
    }
}