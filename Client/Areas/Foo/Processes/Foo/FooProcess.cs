﻿using System;
using ProcessFramework.Client.Areas.Foo.Controllers;
using ProcessFramework.Client.Areas.Foo.Models;
using ProcessFramework.Client.Common.Process;

namespace ProcessFramework.Client.Areas.Foo.Processes.Foo
{
    [Process("Fooing and barring")]
    [Serializable]
    public partial class FooProcess : IProcess
    {
        [Serializable]
        [ConstrainDynamicTransitionBase]
        public abstract class Step2Base : ProcessStateBase<FooProcessModel>
        { }

        [InitialState(typeof(FooApiController))]
        [View(typeof(FooController), FooController.ActionNameConstants.Index)]
        [State(Label = "Step 1", Order = 0)]
        public interface IStep1 : IProcessState
        {
            [Transition(typeof(FooApiController), 
                FooApiController.ActionNameConstants.DoAServerSideBranch,
                TransitionStereoType.Next)]
            Step2Base Branch(IndexViewModel viewModel);
        }

        [View(typeof(FooController), FooController.ActionNameConstants.Step2A)]
        [State(Label = "Step 2A", Order = 1)]
        public interface IStep2A : IProcessState
        {
            [Transition(typeof(FooApiController), 
                FooApiController.ActionNameConstants.Next, 
                TransitionStereoType.Next)]
            IStep3 CarryOn();

            [Transition(typeof(FooApiController), 
                FooApiController.ActionNameConstants.BackToIndex, 
                TransitionStereoType.Back)]
            IStep1 Back();
        }

        [View(typeof(FooController), FooController.ActionNameConstants.Step2B)]
        [State(Label = "Step 2B", Order = 2)]
        public interface IStep2B : IProcessState
        {
            [Transition(typeof(FooApiController), FooApiController.ActionNameConstants.Next,
                TransitionStereoType.Next)]
            IStep3 NextPlease();

            [Transition(typeof(FooApiController), FooApiController.ActionNameConstants.Back,
                TransitionStereoType.Back)]
            IStep2A Back();

            [Transition(typeof(FooApiController), FooApiController.ActionNameConstants.Exit,
                TransitionStereoType.Exit)]
            WrapAroundProcessCompletedResult EndWithNew();
        }

        [State(Label = "Step 3", Order = 3)]
        [View(typeof(FooController), FooController.ActionNameConstants.Step3)]
        public interface IStep3 : IProcessState
        {
            [Transition(typeof(FooApiController), FooApiController.ActionNameConstants.PostFileDownload, TransitionStereoType.FileDownload)]
            IStep3 ProduceDownload();

            [Transition(typeof(FooApiController), FooApiController.ActionNameConstants.Back, TransitionStereoType.Back)]
            IProcessState Back();
            
            [Transition(typeof(FooApiController), FooApiController.ActionNameConstants.Exit,
                TransitionStereoType.Exit)]
            ProcessCompletedResult EndWithClose();
        }
    }
}