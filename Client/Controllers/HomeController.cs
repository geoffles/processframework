﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using ProcessFramework.Client.Common.Context;
using ProcessFramework.Client.Common.Helpers;
using ProcessFramework.Client.Common.Process;
using ProcessFramework.Client.Common.Process.Menu;
using ProcessFramework.Client.Common.Security;
using ProcessFramework.Client.Models.Home;
using MenuGroup = ProcessFramework.Client.Models.Home.MenuGroup;

namespace ProcessFramework.Client.Controllers
{
    public partial class HomeController : Controller
    {
        /// <summary>
        /// Determines whether the current user has access to the process.
        /// </summary>
        private bool IsAccessable(ProcessGrouping item)
        {
            var user = ControllerContext.HttpContext.User;

            var customAttributes = item.Process.GetCustomAttributes(typeof(ProcessAttribute), true);
            var processAttribute = customAttributes.Any() ? ((ProcessAttribute) customAttributes.First()) : null;
            return  processAttribute == null || WebContext.HasPermissions(user.Identity, processAttribute.RequiredPermissions);
        }

        private static WebContext WebContext
        {
            get { return ObjectFactory.Resolve<WebContext>(); }
        }

        private static IAuthorizationService AuthorizationService
        {
            get { return ObjectFactory.Resolve<IAuthorizationService>(); }
        }

        /// <summary>
        /// The main landing page.
        /// </summary>
        public virtual ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Produce a view with a list of information for resuming processes in session.
        /// </summary>
        public virtual ActionResult ProcessResumeInfo()
        {
            IList<ProcessResumeInfo> model = ProcessManager.Instance.GetProcessResumeInfo();
            return View(model);
        }


        public virtual ActionResult Tab()
        {
            return View();
        }

        /// <summary>
        /// Get a blank view which does not include the layout.
        /// </summary>
        public virtual ActionResult BlankView()
        {
            return View();
        }

        /// <summary>
        /// Collects the list of menu groups for display and checks the authourisation.
        /// </summary>
        public virtual ActionResult Nav()
        {
            IList<IMenuConfiguration> menuConfigurations = ProcessManager.MenuConfigurations();
            var menuGroups = menuConfigurations.SelectMany(p => p.GetMenuGroups());

            SecurityHelper.EnsureUserPermissionsLoaded(ControllerContext, WebContext, AuthorizationService);
            IEnumerable<MenuGroup> model  = menuConfigurations
                .SelectMany(p => p.GetMenuItems())
                .Where(IsAccessable)
                .GroupBy(p => p.GroupKey)
                .Join(menuGroups,
                      items => items.Key,
                      group => group.GroupKey,
                      (items, group) =>
                      new MenuGroup
                      {
                          Label = group.Label,
                          GroupItems = items
                              .OrderBy(p => p.Order)
                              .ThenBy(p => p.Label)
                              .Select(i =>
                                      new MenuItem
                                      {
                                          Label = i.Label,
                                          ProcessMetaModel =
                                              ProcessManager.Instance.GetProcessIntialiser(i.Process)
                                      })
                              .ToList()
                      });

            return View(model);
        }

        /// <summary>
        /// Removes the tab from the list of processes.
        /// </summary>
        public virtual JsonResult CancelTab(Guid id)
        {
            ProcessManager.Instance.RemoveProcess(id);
            return Json(new {Removed = true}, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Touches the session to renew the timeout.
        /// </summary>
        public virtual ActionResult RenewSession()
        {
            return Json(new { Renew = true }, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult FileDownloadFrame()
        {
            return View();
        }
    }
}
