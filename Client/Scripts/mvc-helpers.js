﻿/// <reference path="~/Scripts/jquery-1.7.1-vsdoc.js" />
/// <reference path="~/Scripts/date-helpers.js" />

var MvcHelpers = {

    clone: function (obj) {
        // Handle the 3 simple types, and null or undefined
        if (null == obj || "object" != typeof obj) return obj;

        // Handle Date
        var copy;
        if (obj instanceof Date) {
            copy = new Date();
            copy.setTime(obj.getTime());
            return copy;
        }

        // Handle Array
        if (obj instanceof Array) {
            copy = [];
            for (var i = 0, len = obj.length; i < len; ++i) {
                copy[i] = MvcHelpers.clone(obj[i]);
            }
            return copy;
        }

        // Handle Object
        if (obj instanceof Object) {
            copy = {};
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = MvcHelpers.clone(obj[attr]);
            }
            return copy;
        }

        throw new Error("Unable to copy obj! Its type isn't supported.");
    },

    makeJsonObjectMvcFriendly: function (object) {
        if ($.type(object) === 'array') {
            for (var arrayIndex in object) {
                MvcHelpers.makeJsonObjectMvcFriendly(object[arrayIndex]);
            }
        }
        else {
            for (var prop in object) {
                var propertyType = $.type(object[prop]);

                switch (propertyType) {
                    case 'array':
                    case 'object':
                        MvcHelpers.makeJsonObjectMvcFriendly(object[prop]);
                        break;

                    case 'string':
                        var dt = DateHelpers.toDate(object[prop]);
                        if (dt) {
                            object[prop] = DateHelpers.toStandardDateString(object[prop]);
                        }
                        break;

                    case 'date':
                        object[prop] = DateHelpers.toStandardDateString(object[prop]);
                        break;

                    default:
                        alert("Cannot deal with type " + propertyType);
                }
            }
        }
    },

    mvcStringify: function (object) {
        var text = null;
        if (object) {
            var clonedObject = MvcHelpers.clone(object);
            MvcHelpers.makeJsonObjectMvcFriendly(clonedObject);
            text = JSON.stringify(clonedObject);
        }
        return text;
    },

    mvcToJsonDateReplacer: function (key, value) {
        var propertyType = $.type(value);
        switch (propertyType) {
            case 'string':
                var dt = DateHelpers.toDate(value);
                if (dt) {
                    return DateHelpers.toStandardDateString(value, true);
                }
                break;
            case 'date':
                return DateHelpers.toStandardDateString(value, true);
        }
        return value;
    }
};
