﻿/// <reference path="~/Scripts/jquery-1.7.1-vsdoc.js" />

var DateHelpers = (function () {

    var errorTextInvalidDate = 'Invalid Date';

    return {

        Months: {
            JAN: 0,
            FEB: 1,
            MAR: 2,
            APR: 3,
            MAY: 4,
            JUN: 5,
            JUL: 6,
            AUG: 7,
            SEP: 8,
            OCT: 9,
            NOV: 10,
            DEC: 11
        },

        MIN_DATE: null,
        MAX_DATE: null,
        DAYS_IN_WEEK: 7,

        isBetween: function (date, beginDate, endDate) {
            beginDate = DateHelpers.toDate(beginDate) || DateHelpers.MIN_DATE;
            endDate = DateHelpers.toDate(endDate) || DateHelpers.MAX_DATE;
            return beginDate <= date && date < endDate;
        },

        today: function () {
            var nowDate = new Date();
            return DateHelpers.toDate(nowDate, true);
        },

        sortAsc: function (date1, date2) {
            // This is a comparison function that will result in dates being sorted in
            // ASCENDING order. As you can see, JavaScript's native comparison operators
            // can be used to compare dates. This was news to me.

            if (date1 > date2) return 1;
            if (date1 < date2) return -1;
            return 0;
        },

        sortDesc: function (date1, date2) {
            // This is a comparison function that will result in dates being sorted in
            // DESCENDING order.

            if (date1 > date2) return -1;
            if (date1 < date2) return 1;
            return 0;
        },

        toStandardDateString: function (dt, excludeTime) {
            var datetime = null;
            if (dt != null) {
                if (typeof dt == 'string') {
                    var dateText = dt;
                    if (dt[0] == '/') {
                        dateText = parseInt(dt.replace(/(^.*\()|([+-]\d*$)/g, ''));
                    }
                    datetime = new Date(dateText);
                    if (errorTextInvalidDate == datetime) {
                        datetime = null;
                    }
                } else {
                    datetime = dt;
                }
            }

            if (datetime) {
                datetime = excludeTime ? datetime.toLocaleDateString() : datetime.toLocaleString();
            } else {
                datetime = null;
            }

            return datetime;
        },

        clearTime: function (date) {
            if (date) {
                date.setHours(0);
                date.setMinutes(0);
                date.setSeconds(0);
            }
            return date;
        },

        toDate: function (date, excludeTime) {
            var resultDate = null;
            if (date != null) {
                var dateType = $.type(date);
                if (dateType === 'date') {
                    resultDate = date;
                } else if (dateType === 'string') {
                    var standardDateString = DateHelpers.toStandardDateString(date);
                    if (standardDateString) {
                        resultDate = new Date(Date.parse(standardDateString));
                    }
                }
            }

            if (excludeTime && resultDate != null) {
                var year = resultDate.getFullYear();
                var month = resultDate.getMonth();
                var day = resultDate.getDate();
                resultDate = new Date(year, month, day, 0, 0, 0, 0);
            }

            return resultDate;
        },

        dateEquals: function (date1, date2) {
            var date1Time = DateHelpers.getTimeValue(date1);
            var date2Time = DateHelpers.getTimeValue(date2);

            var areDatesEqual = (date1Time == date2Time);

            return areDatesEqual;
        },

        getTimeValue: function (date) {
            var timeValue = 0;
            if (date) {
                var dateType = $.type(date);
                if (dateType != 'date') {
                    throw String.format('Unexpected type \'{0}\' - expected date type.', dateType);
                }
                timeValue = date.getTime();
            }
            return timeValue;
        },

        addMilliseconds: function (date, ms) {
            ms = parseInt(ms);
            if (isNaN(ms)) {
                throw Error('Expected ms to specified.');
            }
            var dateWithAddedMSs = new Date(date);
            dateWithAddedMSs.setMilliseconds(dateWithAddedMSs.getMilliseconds() + ms);
            return dateWithAddedMSs;
        },

        addSeconds: function (date, seconds) {
            seconds = parseInt(seconds);
            if (isNaN(seconds)) {
                throw Error('Expected seconds to specified.');
            }
            var dateWithAddedSeconds = new Date(date);
            dateWithAddedSeconds.setSeconds(dateWithAddedSeconds.getSeconds() + seconds);
            return dateWithAddedSeconds;
        },

        addDays: function (date, days) {
            days = parseInt(days);
            if (isNaN(days)) {
                throw Error('Expected days to specified.');
            }
            var dateWithAddedDays = new Date(date);
            dateWithAddedDays.setDate(dateWithAddedDays.getDate() + days);
            return dateWithAddedDays;
        },

        addYears: function (date, years) {
            years = parseInt(years);
            if (isNaN(years)) {
                throw Error('Expected years to specified.');
            }
            var dateWithAddedYears = new Date(date);
            var newYear = dateWithAddedYears.getFullYear() + years;
            dateWithAddedYears.setFullYear(newYear);
            return dateWithAddedYears;
        },

        dateTimeFormat: null,
        dateFormat: null,
        timeFormat: null,

        formatDateTime: function (dateTime, excludeTime) {
            var formattedDateTime = null;
            if (dateTime != null) {
                var dateTimeType = $.type(dateTime);
                if (dateTimeType != 'date') {
                    throw String.format('Unexpected object type \'{0}\' - expected Date type.', dateTimeType);
                }
                var dateTimeFormat = '{0:'.concat(excludeTime ? DateHelpers.dateFormat : DateHelpers.dateTimeFormat).concat('}');
                formattedDateTime = $.telerik.formatString(dateTimeFormat, dateTime);
            }
            return formattedDateTime;
        },

        centerAlign: function (text) {
            var $div = $('<div>');
            $div.css('text-align', 'center');
            $div.html(text);
            return $div.outerHTML();
        },

        userFriendlyDate: function (date, ifNullText) {
            if (!date) {
                if (ifNullText !== undefined) {
                    date = ifNullText;
                }
            }
            else {
                date = DateHelpers.formatDateTime(DateHelpers.toDate(date), true);
            }
            return date;
        }
    };
})();

