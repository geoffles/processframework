//Start paging FX

/// <summary>
/// Creates a paging wrapper with a core datasource.
/// </summary>
/// options:
/// (required) function loadPageInto(pager, whenDone)
function Pager(o) {
    var self = {};
    self.options = o;
    self.pageNumber = ko.observable(1);
    self.items = ko.observableArray();
    self.pageCount = ko.computed(function () { return self.options.pageCount(); });
    var pageIsLoading = false;

    self.peekIsLocked = function () {
        return pageIsLoading;
    };

    function doneLoading() {
        pageIsLoading = false;
    }

    function loadLock() {
        if (!pageIsLoading) {
            pageIsLoading = true;
            return true;
        } else {
            return false;
        }
    }

    self.next = function () {
        if (self.pageNumber() >= self.pageCount()) {
            return false;
        }
        if (!loadLock()) {
            return false;
        }

        self.pageNumber(self.pageNumber() + 1);
        self.options.loadPageInto(self, function () { doneLoading(); });
        return true;
    };

    self.first = function () {
        if (self.pageNumber() != 1) {
            self.pageNumber(1);
            self.options.loadPageInto(self);
            return true;
        }
        return false;
    };

    self.prev = function () {
        if (self.pageNumber() <= 1) {
            return false;
        }
        if (!loadLock()) {
            return false;
        }

        self.pageNumber(self.pageNumber() - 1);
        self.options.loadPageInto(self, function () { doneLoading(); });
        return true;
    };

    self.loadCurrentPage = function () {
        self.options.loadPageInto(self);
    };

    self.last = function () {
        if (isNumber(self.pageCount()) && self.pageNumber() != self.pageCount()) {
            self.pageNumber(self.pageCount());
            self.options.loadPageInto(self);
            return true;
        }
        return false;
    };

    self.sortDirection = null;
    self.sortField = null;

    self.updateSorting = function () { self.loadCurrentPage(); };

    self.applyPagedSortingToColumnHeaders = function (element) {
        element.applySortingToColumnHeaders(self);
    };

    return self;
};

/// <summary>
/// Creates a pager which pages on a given list with page size.
/// </summary>
function ArrayPager(array, pageSize) {
    var options = {
        pageSize: pageSize
    };

    options.loadPageInto = function (pager, onCompleted) {
        var items = pager.items;
        var pageNumber = pager.pageNumber();
        items.removeAll();

        var pageSrc = array.slice(0);
        if (typeof (options.sortComparerFactory) != 'undefined' && pager.sortField != null && pager.sortDirection != null) {
            pageSrc.sort(options.sortComparerFactory(pager.sortField, pager.sortDirection));
        }

        var pageStart = options.pageSize * (pageNumber - 1);
        for (var i = 0; i < pageSize && i < pageSrc.length - pageStart; i++) {
            items.push(pageSrc[i + pageStart]);
        }
        if (typeof (onCompleted) != 'undefined') {
            onCompleted();
        }
    };

    options.pageCount = ko.observable(Math.ceil(array.length / pageSize));

    return new Pager(options);
}

/// <summary>
/// Creates a pager which fetches individual pages from a delegate.
/// </summary>
/// config:
/// (required) function loadPage(pageNumber, whenDone) 	- fetch page delegate
/// (optional) function onLoadPage() 					- invoked before loading a page
/// (optional) function onPageLoaded()					- invoked when the page has finshed loaded
function ServerPager(config) {
    var options = {
        pageSize: config.pageSize
    };

    options.loadPageInto = function (pager, onCompleted) {
        var items = pager.items;
        var pageNumber = pager.pageNumber();

        items.removeAll();

        if (typeof (config.onLoadPage) != 'undefined') {
            config.onLoadPage();
        }

        config.loadPage(pageNumber, function (page) {
            if (page.HasData) {
                applicationContext.setTimeoutWarningTimer();
                setPageCount(page.TotalCount);
                ko.utils.arrayForEach(page.Data, function (i) {
                    items.push(i);
                });

                if (typeof (config.onPageLoaded) != 'undefined') {
                    config.onPageLoaded();
                }
            } else {
                applicationContext.showErrorBanner("There is no page data.");
            }

            if (typeof (config.onCompleted) != 'undefined') {
                config.onCompleted();
            }

            if (typeof (onCompleted) != 'undefined') {
                onCompleted();
            }
        }, pager.sortField, pager.sortDirection);
    };

    options.pageCount = ko.observable('');

    function setPageCount(totalRecords) {
        options.pageCount(Math.ceil(totalRecords / options.pageSize));
    }

    return new Pager(options);
}

///<summary>
/// A container for items in a list that had changed.
///</summary>
///<param name="itemComparer">
///    (Required) selector for the identifying field of each item.
///    args: (exisitingItem, newItem)
///</param>
///<param name="updater">
///     (optional) The function to call when coercing page items with dirty data. 
///     args: (cleanItems, dirtyItem, index)
///     default: replaces clean item with dirty item
///</param>
///<param name="onChanged">Fired when the dirty items list has changed</param>

function DirtyItems(itemComparer, updater, onChanged) {
    if (!updater) {
        updater = function (cleanItems, dirtyItem, index) {
            cleanItems[index] = dirtyItem;
        };
    }

    var dirtyItems = new Array();
    var self = {};

    self.push = function (item) {
        for (var i = 0; i < dirtyItems.length; i++) {
            if (itemComparer(dirtyItems[i], item)) {
                dirtyItems.splice(i, 1);
                break;
            }
        }
        dirtyItems.push(item);

        if (onChanged) {
            onChanged();
        }
    };

    self.remove = function (item) {
        for (var i = 0; i < dirtyItems.length; i++) {
            if (itemComparer(dirtyItems[i], item)) {
                dirtyItems.splice(i, 1);
                break;
            }
        }

        if (onChanged) {
            onChanged();
        }
    };

    self.applyChanges = function (serverItems) {
        var i, j;
        for (i = 0; i < serverItems.length; i++) {
            for (j = 0; j < dirtyItems.length; j++) {
                if (itemComparer(serverItems[i], dirtyItems[j])) {
                    updater(serverItems, dirtyItems[j], i);
                    break;
                }
            }
        }
    };

    self.dirtyItems = function () {
        return dirtyItems;
    };

    return self;
}
//end paging FX

//start editor FX

///<summary>
///
///</summary>
function SubscribableTrigger() {
    var listeners = new Array();
    var self = {};
    self.subscribe = function (f) {
        listeners.push(f);
    };

    self.trigger = function (arg) {
        for (var f in listeners) {
            listeners[f](arg);
        }
    };

    return self;
}

///<summary>
///</summary>
/// (required) editItemFields : array containing field names to copied into the edit template. Fields can, but are not required to be observables.
/// (required) targetObservable : the observable which represents the selected item.
/// (optional) onItemApplyChange(item) : executed when applyChanges is called on an edit item with the target item.
function SelectedItemHelper(options) {
    var editItemHelper = {};
    var context = {};

    function unwrap(obj) {
        if (ko.isObservable(obj)) {
            return obj();
        } else {
            return obj;
        }
    }

    editItemHelper.clearSelect = function () {
        context.clearTrigger.trigger();
        options.targetObservable(editItemHelper.createNullItem());
    };

    editItemHelper.applySelectBehaviour = function (items) {
        context.clearTrigger = context.clearTrigger || new SubscribableTrigger();
        ko.utils.arrayForEach(items, function (i) {
            i.isSelected = ko.observable(false);
            context.clearTrigger.subscribe(function (arg) {
                if (arg != i) {
                    i.isSelected(false);
                }
            });
            i.select = function () {
                context.clearTrigger.trigger(i);
                i.isSelected(true);
                options.targetObservable(editItemHelper.createEditItem(i));
            };
        });
    };

    editItemHelper.createNullItem = function (obj) {
        var nullItem = {};
        nullItem.IsSet = false;

        for (var i = 0; i < options.editItemFields.length; i++) {
            nullItem[options.editItemFields[i]] = null;
        }

        nullItem.applyChanges = function () {
        };
        nullItem.cancel = function () {
        };

        return nullItem;
    };

    editItemHelper.createEditItem = function (obj) {
        var editItem = {};
        editItem.IsSet = true;

        for (var i = 0; i < options.editItemFields.length; i++) {
            var fieldName = options.editItemFields[i];
            editItem[fieldName] = ko.observable(unwrap(obj[fieldName]));
        }

        editItem.applyChanges = function () {
            for (var i = 0; i < options.editItemFields.length; i++) {
                var fieldName = options.editItemFields[i];
                var value = editItem[fieldName]();
                if (ko.isObservable(obj[fieldName])) {
                    obj[fieldName](value);
                } else {
                    obj[fieldName] = value;
                }
            }
            if (typeof (options.onItemApplyChange) != 'undefined') {
                options.onItemApplyChange(obj);
            }
            context.clearTrigger.trigger();
            options.targetObservable(editItemHelper.createNullItem());
        };

        editItem.cancel = function () {
            context.clearTrigger.trigger();
            options.targetObservable(editItemHelper.createNullItem());
        };

        return editItem;
    };

    return editItemHelper;
}

//end editor FX

//start global helper FX
function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

$.fn.gridPager = function (pager, buttonClass) {
    if (typeof (buttonClass) == 'undefined') {
        classStr = '';
    } else {
        classStr = 'class="' + buttonClass + '"';
    }
    var html = '<button ' + classStr + ' data-bind="click: pager.first">&lt;&lt;</button>' +
        '<button ' + classStr + ' data-bind="click: pager.prev">&lt;</button>' +
            '<span data-bind="text:pager.pageNumber" data-type="pageNumber" class="pagerSpan"></span>' +
                '&nbsp;of&nbsp;' +
                    '<span data-bind="text:pager.pageCount" data-type="pageCount" class="pagerSpan"></span>' +
                        '<button ' + classStr + ' data-bind="click: pager.next">&gt;</button>' +
                            '<button ' + classStr + ' data-bind="click: pager.last">&gt;&gt;</button>';

    this.html(html);
};

function StandaloneSortConfig(array) {
    function compare(a, b) {
        a = ko.utils.unwrapObservable(a);
        b = ko.utils.unwrapObservable(b);

        var afield = a[sortConfig.sortField];
        var bfield = b[sortConfig.sortField];

        afield = ko.utils.unwrapObservable(afield);
        bfield = ko.utils.unwrapObservable(bfield);

        if (afield == bfield) {
            return 0;
        }

        return afield > bfield ? 1 : -1;
    }

    var count = 0;
    ko.utils.arrayForEach(ko.utils.unwrapObservable(array), function (item) {
        item._servedModelOrder = count;
        count += 1;
    });

    var sortConfig = {
        sortField: null,
        sortDirection: null,
        updateSorting: function () {
            var sortComparer;
            if (!this.sortField) {
                sortComparer = function (a, b) {
                    return a._servedModelOrder - b._servedModelOrder;
                };
            } else {
                if (this.sortDirection == 'Ascending') {
                    sortComparer = function (a, b) {
                        return compare(a, b);
                    };
                } else {
                    sortComparer = function (a, b) {
                        return compare(b, a);
                    };
                }

            }

            //Perform the sorting
            array.sort(sortComparer);
        }
    };
    return sortConfig;
}

///<summary>Configures the column headers for sorting</summary>
///<param name="sortConfig">Configuration object for sorting
/// (required) sortField        : get and set the current sort field
/// (required) sortDirection    : get and set the sort direction
/// (required) updateSorting    : delegate to perform sorting
///</param>
$.fn.applySortingToColumnHeaders = function (sortData) {
    var element = this;
    var asc = 'Ascending';
    var dsc = 'Descending';
    var headers = element.find('th[data-sort]');

    headers.append('<span style="float:right"></span>');

    headers.click(function (e) {
        var field = $(this).attr('data-sort');
        if (field == sortData.sortField) {
            switch (sortData.sortDirection) {
                case asc:
                    sortData.sortDirection = dsc;
                    $(this).find('span').html('&or;');
                    break;
                case dsc:
                    sortData.sortDirection = null;
                    sortData.sortField = null;
                    $(this).find('span').text('');
                    break;
                default:
                    sortData.sortDirection = asc;
                    $(this).find('span').html('&and;');
                    break;
            }
        } else {
            sortData.sortField = field;
            sortData.sortDirection = asc;
            $(this).siblings().find('span').text('');
            $(this).find('span').html('&and;');
        }
        sortData.updateSorting();
    });
};

$.fn.inlineBusyIndicator = function () {
    var html = '<img src="' + applicationContext.baseUrl + 'Content/Images/ajax-Loader.gif" />';
    var img = $(html);
    img.hide();
    this.before(img);
    return { original: this, busy: img };
};

$.fn.createLoadingIndicator = function () {
    var html = '<div class="loadingIndicator" style="display:none"><img src="/Content/Images/ajax-Loader.gif" /><p>Loading...</p></div>';

    var element = $(html);
    this.append(element);
    return element;
};

/// <summary>
/// Sets up the grid to have a scroll bar and be the requested width.
/// </summary>
/// <param name="reqWidth">(optional) Defaults to 100%. Can also specify units.</param>
$.fn.setScrollableGridMetrics = function (reqWidth) {
    var element = this;

    if (!reqWidth) {
        reqWidth = '100%';
    }

    //get the outer width of all the cells
    element.width(reqWidth);
    var innerWidth = element.width() - getScrollbarWidth() - 2;

    var headTable = element.find('div[data-type=head]>table');
    headTable.width(innerWidth);

    var bodyTable = element.find('div[data-type=body]>table');
    bodyTable.width(innerWidth);

    var ths = element.find('div[data-type=head]>table>thead>tr>th');
    var tds = element.find('div[data-type=body]>table>tbody>tr>td');
    $.each(ths, function (index, th) {
        $(tds[index]).attr('style', $(th).attr('style'));
    });
    tds = null;
};

var getScrollbarWidth = (function () {
    function calculateWidth() {
        var div, body, W = window.browserScrollbarWidth;
        if (W === undefined) {
            body = document.body, div = document.createElement('div');
            div.innerHTML = '<div style="width: 50px; height: 50px; position: absolute; left: -100px; top: -100px; overflow: auto;"><div style="width: 1px; height: 100px;"></div></div>';
            div = div.firstChild;
            body.appendChild(div);
            W = window.browserScrollbarWidth = div.offsetWidth - div.clientWidth;
            body.removeChild(div);
        }
        return W;
    }

    var scrollBarWidth = null;
    return function () {
        if (scrollBarWidth == null) {
            scrollBarWidth = calculateWidth();
        }
        return scrollBarWidth;
    };
})();

//end global helper FX
