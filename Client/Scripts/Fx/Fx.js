﻿String.prototype.format = function () {
    var formatted = this;
    for (var arg in arguments) {
        formatted = formatted.replace("{" + arg + "}", arguments[arg]);
    }
    return formatted;
};

if (!String.prototype.isEmptyOrWhitespace) {
    String.prototype.isEmptyOrWhitespace = function () {
        return this.replace(/^\s+|\s+$/g, '') == "";
    }
}

String.prototype.toPercent = function () {
    var formatted = this;

    function padZero(obj) {
        if (obj.length == 2) {
            return "";
        } else if (obj.length == 0) {
            obj = ".00";
        } else if (obj.length == 1) {
            obj = "0";
        }

        return obj;
    }

    var percentagePart = "";
    if (formatted.indexOf(".") != -1) {
        percentagePart = formatted.substring(formatted.indexOf('.') + 1);
    }

    formatted = formatted + padZero(percentagePart) + "%";

    return formatted;
};

String.prototype.toCurrency = function () {
    var formatted = this;

    function padZero(obj) {
        if (obj.length == 2) {
            return "";
        }
        else if (obj.length == 0) {
            obj = ".00";
        } else if (obj.length == 1) {
            obj = "0";
        }

        return obj;
    }

    var decimalPart = "";
    if (formatted.indexOf(".") != -1) {
        decimalPart = formatted.substring(formatted.indexOf('.') + 1);
    }

    formatted = "R " + formatted + padZero(decimalPart);

    return formatted;
};

Date.prototype.format = function () {

    function padZero(obj) {
        obj = obj + '';
        if (obj.length == 1) {
            obj = "0" + obj;
        }
        return obj;
    }

    var year = this.getFullYear();
    var month = padZero(this.getMonth() + 1); //getMonth is zero-based
    var day = padZero(this.getDate());

    return year + '/' + month + '/' + day;
};

// Array Prototypes
if (!Array.prototype.filter) {
    Array.prototype.filter = function (fun /*, thisArg */) {
        "use strict";

        if (this === void 0 || this === null)
            throw new TypeError();

        var t = Object(this);
        var len = t.length >>> 0;
        if (typeof fun !== "function")
            throw new TypeError();

        var res = [];
        var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
        for (var i = 0; i < len; i++) {
            if (i in t) {
                var val = t[i];

                // NOTE: Technically this should Object.defineProperty at
                //       the next index, as push can be affected by
                //       properties on Object.prototype and Array.prototype.
                //       But that method's new, and collisions should be
                //       rare, so use the more-compatible alternative.
                if (fun.call(thisArg, val, i, t))
                    res.push(val);
            }
        }

        return res;
    };
}

if (!Array.prototype.find) {
    Object.defineProperty(Array.prototype, 'find', {
        enumerable: false,
        configurable: true,
        writable: true,
        value: function (predicate) {
            if (this == null) {
                throw new TypeError('Array.prototype.find called on null or undefined');
            }
            if (typeof predicate !== 'function') {
                throw new TypeError('predicate must be a function');
            }
            var list = Object(this);
            var length = list.length >>> 0;
            var thisArg = arguments[1];
            var value;

            for (var i = 0; i < length; i++) {
                if (i in list) {
                    value = list[i];
                    if (predicate.call(thisArg, value, i, list)) {
                        return value;
                    }
                }
            }
            return undefined;
        }
    });
}

if (!Array.prototype.map) {
    Array.prototype.map = function (fun /*, thisArg */) {
        "use strict";

        if (this === void 0 || this === null)
            throw new TypeError();

        var t = Object(this);
        var len = t.length >>> 0;
        if (typeof fun !== "function")
            throw new TypeError();

        var res = new Array(len);
        var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
        for (var i = 0; i < len; i++) {
            // NOTE: Absolute correctness would demand Object.defineProperty
            //       be used.  But this method is fairly new, and failure is
            //       possible only if Object.prototype or Array.prototype
            //       has a property |i| (very unlikely), so use a less-correct
            //       but more portable alternative.
            if (i in t)
                res[i] = fun.call(thisArg, t[i], i, t);
        }

        return res;
    };
}

function $ViewContext() {
    return applicationContext.activeView().viewContext;
}
function $OnViewReady(a) {
    applicationContext.activeView().onViewReady(a);
}
function $ChangeView(viewUrl, dataUrl, model) {
    applicationContext.activeView().changeCurrentView(viewUrl, dataUrl, model);
}
function $CanCompleteProcess() {
    var activeView = applicationContext.activeView();
    return activeView == null ? false : activeView.validationController.canCompleteProcess();
}


/// <summary> Apply the view model as per the bindings for the tab view. </summary>
/// <param name="viewModel">The model to bind to</param>
/// <param name="element">(Optional)specify a target binding element</param>
function $ApplyViewBindings(viewModel, element) {
    if (typeof (element) == 'undefined') {
        element = $('#tabContent')[0];
    }
    ko.applyBindings(viewModel, element);
}
function $Validate(model) {
    var validationResult = true;
    for (var f in model) {
        var field = model[f];
        //check for is valid function and invoke if it exists.
        if (field != null && $.isFunction(field.isValid)) {
            var fieldValid = field.isValid();
            if (!fieldValid) {
                validationResult = false;
            }
        }
    }
    return validationResult;
}
function Latch() {
    var locked = false;

    this.withLock = function (func) {
        if (locked) {
            return false;
        } else {
            locked = true;
            func();
            locked = false;
            return true;
        }

    };
    //returns true if the latch is not set;
    this.peek = function () {
        return !locked;
    };
}
function resetTableBodyColumnsToFixed() {
    setTimeout(function () {
        $('.gridBody').css('table-layout', 'auto');
        setTimeout(function () { $('.gridBody').css('table-layout', 'fixed'); }, 10);
    }, 10);
}
function toggleGroup(e) {
    var element = $(e);

    element.toggleClass('menuGroup-open');
    element.next().slideToggle();
}


var $FileDownloadParameters;
var $FileDownloadUrl;
var $FileDownloadTabId;
var $FileDownloadCompleted;
function $InvokeFileDownload(url, parameters, onCompleted) {
    applicationContext.clearErrorBanner();
    $FileDownloadParameters = parameters;
    $FileDownloadUrl = url;
    $FileDownloadTabId = applicationContext.activeView().tabId();
    $FileDownloadCompleted = onCompleted;

    //Remove an iframe if one exists already.
    $("#dliframe" + $FileDownloadTabId).remove();

    var iframe = document.createElement("iframe");
    iframe.setAttribute('id', 'dliframe' + $FileDownloadTabId);
    iframe.setAttribute('hidden', 'hidden');
    iframe.setAttribute('src', applicationContext.baseUrl + 'home/FileDownloadFrame');

    document.body.appendChild(iframe);

    applicationContext.showLoadingIndicator();

    window.setTimeout(checkDownloadCompletion, 200);
}

function checkDownloadCompletion() {
    var settings = {
        cookieName: "fileDownload",
        cookieValue: $FileDownloadTabId,
        cookiePath: "/"
    };
    var interval;

    interval = window.setInterval(function () {
        var iframe = $("#dliframe" + $FileDownloadTabId);
        if (document.cookie.indexOf(settings.cookieName + "=" + settings.cookieValue) != -1) {
            window.clearInterval(interval);
            document.cookie = settings.cookieName + "=; expires=" + new Date(1000).toUTCString() + "; path=" + settings.cookiePath;
            applicationContext.hideLoadingIndicator();
            if ($FileDownloadCompleted) {
                $FileDownloadCompleted(true);
            }
        } else {

            var response = iframe.contents().find("body").html();

            //check for null, empty string, or the script identifier: these things mean it's still busy
            if (response != null && response != "" && !response.match(/\w*<!-- PageDownloadScriptIdentifier/)) {
                window.clearInterval(interval);
                applicationContext.hideLoadingIndicator();
                //iframe.remove();

                if ($FileDownloadCompleted) {
                    $FileDownloadCompleted(false, response);
                } else {
                    applicationContext.showErrorBanner('A problem occurred preparing the file for download');
                }
            }
        }

    }, 100);
}
