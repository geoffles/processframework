﻿function ApplicationContext() {
    var applicationContext = {};

    applicationContext.activeView = ko.observable();

    applicationContext.showErrorBanner = function (msg, ex) {
        showErrorBanner(msg, ex);
    };

    applicationContext.clearErrorBanner = function () {
        clearErrorBanner();
    };

    applicationContext.display = function (view) {
        var node = $('#tabContent');
        ko.cleanNode(node[0]);
        //$('.k-reset').remove(); //TODO: Remove dead calendars
        node.empty();
        node.html(view);

    };

    applicationContext.tabController = new TabController(applicationContext);
    applicationContext.processTemplate = $('#gridActions').html();
    applicationContext.issuesTemplate = $('#gridIssues').html();

    applicationContext.showLoadingIndicator = function () {
        $('#loadingIndicator').show();
    };

    applicationContext.hideLoadingIndicator = function () {
        $('#loadingIndicator').fadeOut();
    };

    applicationContext.warningDialogTimeoutId = null;

    var countDownId;
    var countDownCancel = false;
    var warningSeconds = 0;
    applicationContext.showTimeoutWarning = function () {
        window.clearTimeout(countDownId);
        countDownCancel = false;
        $('#sessionRefreshError').hide();
        $('#timeoutWarning').fadeIn();

        warningSeconds = 60;

        applicationContext.warningDialogTimeoutId = window.setTimeout(function () {
            applicationContext.hideTimeoutWarning();
        }, warningSeconds * 1000);

        countDown();
    };


    function countDown() {
        var s = (warningSeconds % 60) + '';
        if (s.length < 2) {
            s = "0" + s;
        }
        $('#timeoutWarning').find('#counter').text(Math.floor(warningSeconds / 60) + ':' + s);

        countDownId = window.setTimeout(function () {
            warningSeconds--;
            if (!countDownCancel) {
                countDown();
            } else {
                countDownCancel = false;
            }

        }, 1000);
    }

    applicationContext.hideTimeoutWarning = function (refreshed) {
        if (typeof (refreshed) == 'undefined') {
            refreshed = false;
        }

        window.clearTimeout(applicationContext.warningDialogTimeoutId);
        if (!refreshed) {
            applicationContext.showSessionExpired();
        }
        countDownCancel = true;
        window.clearTimeout(countDownId);
        $('#timeoutWarning').fadeOut();

    };

    applicationContext.showSessionExpired = function () {
        $('body').empty();
        $('body').html('<div class="pfx-error-page-notification">Your session has expired. Click <a href="' + applicationContext.baseUrl + '">here</a> to start a new one.</div>');
    };


    applicationContext.renewSession = function () {
        $('#sessionRefreshError').fadeOut();
        if (applicationContext.refreshSessionBusyIndicator == null) {
            applicationContext.refreshSessionBusyIndicator = $('#sessionRefreshButton').inlineBusyIndicator();
        }
        
        applicationContext.refreshSessionBusyIndicator.original.hide();
        applicationContext.refreshSessionBusyIndicator.busy.show();
        
        $.ajax(applicationContext.baseUrl + "Home/RenewSession",
            {
                success: function () { applicationContext.hideTimeoutWarning(true); },
                error: function () { $('#sessionRefreshError').fadeIn(); },
                complete: function() {
                    applicationContext.refreshSessionBusyIndicator.busy.hide();
                    applicationContext.refreshSessionBusyIndicator.original.show();
                }
            });

    };

    applicationContext.timeoutMinutes = 0; // To Be initialised in _layout

    applicationContext.sessionTimeoutId = null;
    applicationContext.setTimeoutWarningTimer = function () {
        if (applicationContext.sessionTimeoutId != null) {
            window.clearTimeout(applicationContext.sessionTimeoutId);
        }

        var secondsTillWarning = ((applicationContext.timeoutMinutes - 1) * 60) - 10;

        applicationContext.sessionTimeoutId = window.setTimeout(function () {
            applicationContext.showTimeoutWarning();
        }, secondsTillWarning * 1000);
    };


    return applicationContext;
}
