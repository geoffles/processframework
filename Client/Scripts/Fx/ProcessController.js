﻿function ProcessController() {
    var self = {};
    var processViewModel = {
        states: ko.observableArray([]),
        validationState: {}
    };

    function StateItem(item) {
        this.name = item.Name;
        this.isCurrent = item.IsCurrentState;
        this.errorState = ko.computed(function () {
            if (self.stateErrorLevels()) {
                if (self.stateErrorLevels()[item.Name]) {
                    return self.stateErrorLevels()[item.Name];
                }
            }
            return 0;
        });
        return this;
    }

    self.bindProcessState = function (processState) {
        processViewModel.states.removeAll();

        ko.utils.arrayForEach(processState, function (i) {
            processViewModel.states.push(new StateItem(i));
        });
        self.currentState(getCurrentStateName());
        applicationContext.tabController.setTabHeading(self.currentState());

        self.applyKnockoutBindings();

        self.show();
    };

    function getCurrentStateName() {
        var currentState = ko.utils.arrayFirst(processViewModel.states(), function (i) {
            return i.isCurrent;
        });
        return currentState.name;
    };

    self.show = function () {
        node.fadeIn();
    };

    self.close = function () {
        node.fadeOut();
    };

    self.stateErrorLevels = ko.observable();
    self.currentState = ko.observable();

    var node = $('#gridActions');
    self.applyKnockoutBindings = function () {
        node = $('#gridActions');
        if (node.length > 0) {
            ko.cleanNode(node[0]);
            node.empty();
            node.html(applicationContext.processTemplate);

            ko.applyBindings(processViewModel, $('#gridActions')[0]);
        }
    };

    self.applyKnockoutBindings();

    self.setCurrentStep = function (stepNumber) {
        var i, newStateModel = new Array();

        for (i = 0; i < processViewModel.states().length; i++) {
            var state = processViewModel.states()[i];
            if (i == stepNumber) {
                state.isCurrent = true;
                self.currentState(state);
            }
            else {
                state.isCurrent = false;
            }

            newStateModel.push(state);
        }
        processViewModel.states.removeAll();
        processViewModel.states(newStateModel);
    };

    return self;
}