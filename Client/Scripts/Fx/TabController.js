﻿function Tab(tabInfo, activeTabId) {
    var self = this;
    self.viewController = new ViewController();
    if (tabInfo.tabId) {
        self.viewController.tabId(tabInfo.tabId);
    }
    self.viewController.viewUrl = tabInfo.viewUrl;
    self.viewController.dataUrl = tabInfo.dataUrl;

    self.tabId = ko.computed(function () {
        return self.viewController.tabId();
    });

    self.label = ko.observable(" ");
    if (tabInfo.label) {
        self.label(tabInfo.label);
    }

    self.description = ko.observable(" ");
    if (tabInfo.description) {
        self.description(tabInfo.description);
    }

    self.isActive = ko.computed(function () {
        return activeTabId() == self.tabId();
    });

    return self;
}

function TabController(applicationContext) {
    var tabController = {};

    var activeTabId = ko.observable();
    tabController.updateTabId = function () {
        activeTabId(window.location.hash.substring(1));
    };
    window.addEventListener("hashchange", tabController.updateTabId, false);


    tabController.tabs = ko.observableArray();

    tabController.pushNewTab = function (viewUrl, dataUrl) {
        var tab = new Tab({
            viewUrl: viewUrl,
            dataUrl: dataUrl
        }, activeTabId);

        tabController.tabs.push(tab);
        applicationContext.activeView(tab.viewController);
        tab.viewController.changeCurrentView(tab.viewController.viewUrl, tab.viewController.dataUrl);
    };

    tabController.setTabHeading = function (text) {
        var heading = $('.task-tab-content-heading-section h2');

        heading.text(text);
        heading.parent().parent().fadeIn();

        getTab().label(text);
    };

    tabController.setTabDescription = function (text) {
        getTab().description(text);
    };

    tabController.hideHeading = function () {
        $('div.task-tab-content-heading-section').fadeOut();
    };

    ///<summary>
    ///</summary>
    ///<param name="p">{viewUrl, dataUrl, tabId}</param>
    tabController.pushRefreshedTab = function (p) {
        tabController.tabs.push(new Tab(p, activeTabId));
    };

    tabController.loadTabFromId = function (id) {
        var tab = getTab(id);
        if (tab) {
            applicationContext.activeView(tab.viewController);
            tab.viewController.switchToTab(tab.viewController.viewUrl, tab.viewController.dataUrl);
        }
    };

    tabController.closeTab = function (id) {
        var tab = getTab(id);

        var index = tabController.tabs.indexOf(tab);
        tabController.tabs.splice(index, 1);

        if (tab.isActive()) {
            tab.viewController.closeView();
            if (tabController.tabs().length == 0) {
                applicationContext.activeView(null);
            } else {
                var nextTabIndex = index - 1;
                if (nextTabIndex < 0) {
                    nextTabIndex = 0;
                }
                var nextTab = tabController.tabs()[nextTabIndex];
                tabController.loadTabFromId(nextTab.tabId());
            }
        }
    };

    function getTab(id) {
        if (typeof (id) == 'undefined') {
            id = window.location.hash.substring(1);
        }

        var i, tab;
        for (i = 0; i < tabController.tabs().length; i++) {
            tab = tabController.tabs()[i];
            if (tab.tabId() == id) {
                return tab;
            }
        }
    }

    tabController.cancelTab = function (id) {
        $.ajax(applicationContext.baseUrl + "Home/CancelTab/" + id,
            {
                success: function () {
                    tabController.closeTab(id);
                },
                error: function () {
                    applicationContext.showErrorBanner('An error occurred attempting to close the tab.');
                }
            }
        );
    };

    return tabController;
}