﻿(function () {
    // --- binding handlers --- //

    ko.bindingHandlers.validationMessage = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            $(element).hide();
        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var validationTarget = valueAccessor();
            if (!ko.isObservable(validationTarget.isValid)) {
                throw "The target element does not have an observable 'isValid'";
            }
            var isValid = validationTarget.isValid();
            var message = validationTarget.validationMessage();
            var target = $(element);

            target.text(message);
            target.attr('data-isValid', isValid ? '1' : '0');

            if (isValid) {
                target.fadeOut();
            } else {
                target.fadeIn();
            }
        }
    };

    // --- extenders --- //

    function stubValidationObservables(target, message) {
        target.isValid = ko.observable(false);
        target.validationMessage = ko.observable(message);
    }

    ko.extenders.mandatoryField = function (target, options) {
        var message;
        if (options === true) {
            message = "This field is mandatory.";
        } else {
            message = options.message;
        }

        stubValidationObservables(target, message);

        target.subscribe(function (newValue) {
            var valid = newValue != null && newValue != '';
            target.isValid(valid);
        });

    };

    ko.extenders.mandatoryText = function (target, options) {
        var message;
        if (options === true) {
            message = "This field is mandatory.";
        } else {
            message = options.message;
        }

        stubValidationObservables(target, message);

        target.subscribe(function (newValue) {
            var valid = newValue != null && newValue != '';
            target.isValid(valid);
        });

        return target;
    };

    ko.extenders.mandatoryDate = function (target, options) {
        var message;
        if (options === true) {
            message = "This field is mandatory.";
        } else {
            message = options.message;
        }

        stubValidationObservables(target, message);

        target.subscribe(function (newValue) {
            var valid = newValue != null && ((newValue instanceof Date) || parseDate(newValue));
            target.isValid(valid);
        });

        return target;
    };

    ko.extenders.mandatoryDate = function (target, options) {
        var message;
        if (options === true) {
            message = "This field is mandatory.";
        } else {
            message = options.message;
        }

        stubValidationObservables(target, message);

        target.subscribe(function (newValue) {
            var valid = newValue != null && ((newValue instanceof Date) || parseDate(newValue));
            target.isValid(valid);
        });

        return target;
    };

    ko.extenders.customValidation = function (target, options) {
        function updateIsValid(isValid) {
            target.isValid(isValid);
        }

        function performValidation(newValue) {
            if (options.async) {
                options.validate(newValue, updateIsValid);
            } else {
                updateIsValid(options.validate(newValue));
            }
        }

        stubValidationObservables(target, options.message);

        //initial validation
        performValidation(target());

        target.subscribe(function (newValue) {
            performValidation(newValue);
        });

        return target;
    };

    ko.extenders.manualValidationStub = function (target, message) {
        stubValidationObservables(target, message);

        return target;
    };
})();