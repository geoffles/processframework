﻿function ValidationController() {
    var self = {};
    var validationList = {};
    var validationViewModel = {
        errors: ko.observableArray([]),
        warnings: ko.observableArray([])
    };

    function ValidationItem(item) {
        var validationItem = this;
        validationItem.message = item.Message;
        validationItem.processState = item.ProcessStep;
        validationItem.isVisible = ko.computed(function () {
            return validationItem.processState == self.currentState();
        });
        validationItem.severity = item.Severity;
        validationItem.key = item.Key;
        return validationItem;
    }

    self.bindValidationErrors = function (validationErrors) {
        validationViewModel.errors.removeAll();
        validationViewModel.warnings.removeAll();
        ko.utils.arrayForEach(validationErrors, function (i) {
            var validation = new ValidationItem(i);
            if (validation.severity == 2) {
                validationViewModel.errors.push(validation);
            } else {
                validationViewModel.warnings.push(validation);
            }
        });

        self.processErrorState(calculateValidationSummary());
        self.applyKnockoutBindings();
        self.show();
    };

    function calculateValidationSummary() {
        var validationSummary = {};

        ko.utils.arrayForEach(validationViewModel.warnings(), function (i) {
            validationSummary[i.processState] = 1;
        });
        ko.utils.arrayForEach(validationViewModel.errors(), function (i) {
            validationSummary[i.processState] = 2;
        });
        return validationSummary;
    }

    self.show = function () {
        node.fadeIn();
    };

    self.close = function () {
        node.fadeOut();
    };

    self.currentState = ko.observable();
    self.processErrorState = ko.observable();

    self.canCompleteProcess = ko.computed(function () {
        for (var state in self.processErrorState()) {
            if (self.processErrorState()[state] >= 2) {
                return false;
            }
        }
        return true;
    });

    var node = $('#gridIssues');
    
    self.applyKnockoutBindings = function() {
        node = $('#gridIssues');
        if (node.length > 0) {
            ko.cleanNode(node[0]);
            node.empty();
            node.html(applicationContext.issuesTemplate);

            ko.applyBindings(validationViewModel, $('#gridIssues')[0]);
        }
    };

    self.applyKnockoutBindings();
    

    return self;
}