﻿function ViewController(viewUrl, dataUrl) {
    var viewController = {};
    viewController.tabId = ko.observable();
    viewController.viewUrl = null;
    viewController.dataUrl = null;

    viewController.processController = new ProcessController();
    viewController.validationController = new ValidationController();

    viewController.processController.currentState.subscribe(function (newState) {
        viewController.validationController.currentState(newState);
    });

    viewController.validationController.processErrorState.subscribe(function (newStates) {
        viewController.processController.stateErrorLevels(newStates);
    });


    viewController.viewContext = {};

    viewController.activate = function () {
        if (typeof (viewController.tabId()) == 'undefined') {
            changeCurrentView(viewUrl, dataUrl);
        } else {
            throw 'Tab loading not yet supported';
        }

    };

    viewController.onViewReady = function () {
    };

    viewController.switchToTab = function (viewUrl, dataUrl) {
        var context = {
            viewUrl: null, //this is now deprecated!
            dataUrl: dataUrl,
            model: new Object(),
            fetchOnly: true
        };

        applicationContext.showLoadingIndicator();
        viewController.viewChangeExecutor(context);
    };

    viewController.changeCurrentView = function (viewUrl, dataUrl, model) {
        applicationContext.clearErrorBanner();

        var postModel = model;

        var loading = (function () {
            if ($.isFunction(postModel)) {
                try {
                    postModel = postModel();
                } catch (ex) {
                    applicationContext.showErrorBanner('An error has occurred preparing the server request.');
                    return false;
                }
            }

            //check that the function valid exists and then check that the result is not false
            if (model && model.valid && !model.valid()) {
                $('.field-validation-error[data-isValid=0]').show();
                $('.field-validation-error').animate({ color: '#FFCBC2' }, 100).animate({ color: '#f00' }, 1000);
                return false;
            }
            return true;
        })();

        if (loading) {
            var context = {
                viewUrl: viewUrl,
                dataUrl: dataUrl,
                model: postModel,
                fetchOnly: false
            };

            applicationContext.showLoadingIndicator();
            viewController.viewChangeExecutor(context);
        }
    };

    viewController.viewChangeExecutor = function (context, operation) {
        if (typeof (operation) == 'undefined') {
            operation = "fetchData";
        }

        switch (operation) {
            case 'fetchData':
                {
                    viewController.fetchData(context);
                    break;
                }
            case 'fetchView':
                {
                    if (context.dataResult.success) {
                        viewController.fetchView(context);
                        break;
                    }
                }
            case 'displayResult':
                {
                    viewController.displayResult(context);
                }
        }
    };

    viewController.fetchView = function (context) {
        var fetchContainer = new FetchContainer();
        var view = context.dataResult.requestContent.ViewIdentifier;
        var url = view == null 
            ? applicationContext.baseUrl + 'Home/BlankView'
            : applicationContext.baseUrl + view.AreaName + "/" + view.ControllerName + "/" + view.ActionName;
        
        $.ajax(url, {
            error: function (x, s, e) {
                fetchContainer.processError(x, s, e);
            },
            success: function (d, t, x) {
                applicationContext.setTimeoutWarningTimer();
                fetchContainer.processSuccess(d, t, x);
            },
            complete: function (x, s) {
                context.viewResult = fetchContainer;
                viewController.viewChangeExecutor(context, 'displayResult');
            }
        });
    };

    viewController.fetchData = function (context) {
        var mode, postModel, fetchContainer;
        mode = 'POST';
        postModel = context.model;
        fetchContainer = new FetchContainer();

        if (context.model == null) {
            mode = 'GET';
            postModel = null;
        }


        function fetchDataHeaders(context) {
            var self = {};
            self.Accept = 'application/vnd.process.json';
            if (mode == 'POST') {
                self["Tab-Id"] = viewController.tabId();
            }
            if (context && context.fetchOnly) {
                self["Fetch-Only"] = "true";
            }
            return self;
        }


        $.ajax(context.dataUrl, {
            type: mode,
            data: postModel,
            contentType: 'application/json',
            headers: new fetchDataHeaders(context),
            error: function (x, s, e) {
                fetchContainer.processError(x, s, e);
            },
            success: function (d, t, x) {
                fetchContainer.processSuccess(d, t, x);
            },
            complete: function (x, s) {
                context.dataResult = fetchContainer;
                viewController.viewChangeExecutor(context, 'fetchView');
            }
        });
    };

    viewController.displayResult = function (context) {
        applicationContext.hideLoadingIndicator();
        if (!context.dataResult.success) {
            applicationContext.showErrorBanner('Error fetching the data: ' + context.dataResult.errorText, context.dataResult.errorResponse);
            return;
        }
        
        if (!context.viewResult.success) {
            applicationContext.showErrorBanner('Error fetching the view: ' + context.viewResult.errorText, context.viewResult.errorResponse);
            return;
        }

        viewController.viewContext = {};
        viewController.onViewReady = function (viewDataBind) {
            try {
                var content = context.dataResult.requestContent;
                viewController.tabId(content.TabId);
                window.location.hash = content.TabId;
                applicationContext.tabController.setTabDescription(content.TabDescription);
                viewDataBind(content.Model);
                $('.field-validation-error').hide();
            }
            catch (ex) {
                applicationContext.showErrorBanner("An error occurred displaying the page", ex);
            }
        };


        applicationContext.display(context.viewResult.requestContent);

        viewController.viewUrl = context.viewUrl;
        viewController.dataUrl = context.dataUrl;

        if (context.dataResult.requestContent.CloseTab) {
            if (context.dataResult.requestContent.ReInitialise) {
                var metaModel = context.dataResult.requestContent.Transitions[0];
                var viewUrl = applicationContext.baseUrl + metaModel.View.AreaName + '/' + metaModel.View.ControllerName + '/' + metaModel.View.ActionName;
                var actionUrl = applicationContext.baseUrl + 'api/' + metaModel.Action.AreaName + '/' + metaModel.Action.ControllerName + '/' + metaModel.Action.ActionName;
                viewController.closeView();
                $ChangeView(viewUrl, actionUrl);
                return;
            } else {
                applicationContext.tabController.closeTab(viewController.tabId());
            }
        } else {
            viewController.processController.bindProcessState(context.dataResult.requestContent.ProcessStates);
            viewController.validationController.bindValidationErrors(context.dataResult.requestContent.ObjectiveIssues);
        }
    };

    viewController.closeView = function () {
        viewController.validationController.close();
        viewController.processController.close();
        applicationContext.tabController.hideHeading();
        applicationContext.display('');
    };

    function FetchContainer() {
        var self = this;
        self.success = false;
        self.requestContent = null;
        self.errorText = null;
        self.errorResponse = null;

        self.processError = function (xhr, status, errorThrown) {
            self.success = false;
            switch (status) {
                case "timeout":
                    {
                        self.errorText = "The client is having trouble communicating with the server.";
                        break;
                    }
                case "error":
                    {
                        if (xhr.responseText != null) {
                            try {
                                var tabModel = JSON.parse(xhr.responseText);
                                if (tabModel.ValidationFailure != undefined && tabModel.ValidationFailure == true) {
                                    viewController.processController.bindProcessState(tabModel.ProcessStates);
                                    viewController.validationController.bindValidationErrors(tabModel.ObjectiveIssues);

                                    self.errorText = "A data validity problem has been detected - please see the Object Issues box for more information";
                                    self.errorResponse = xhr.responseText;
                                    break;
                                }
                            } catch (ex) {
                            }
                        }
                        self.errorText = "A server error occurred";
                        if (errorThrown != null) {
                            self.errorText += " - " + errorThrown;
                        }
                        if (xhr.responseText != null) {
                            self.errorResponse = xhr.responseText;
                        }
                        break;
                    }
                case "parsererror":
                    {
                        self.errorText = "A client error has occurred in processing the request";
                        break;
                    }
            }
        };

        self.processSuccess = function (data, textStatus, xhr) {
            self.success = true;
            self.requestContent = data;
        };
    };

    return viewController;
}