using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using ProcessFramework.Client.Common.Process;

namespace ProcessFramework.Client
{
    /// <summary>
    /// The responsibility of this class is to provide the storage of a process manager in the ASP.Net session.
    /// This object should be injected via Spring with a request scope.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-08-22br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public sealed class HttpSessionProcessManagerStateProvider : IProcessManagerStateProvider
    {
        private const string SESSION_KEY = "PROCESS_MANAGER";
        private const string PROCESSES_SESSION_KEY = "PROCESSES";

        private object _lock = new object();
        private volatile ProcessManager _processManagerInstance;
        private volatile IDictionary<Guid, IProcess> _processes;

        /// <summary>
        /// Gets the session instance of the process manager. If the process manager is not in the session, it is created.
        /// </summary>
        public ProcessManager GetProcessManager()
        {
            if (_processManagerInstance == null)
            {
                lock (_lock)
                {
                    var sessionInstance = HttpContext.Current.Session[SESSION_KEY] as ProcessManager;
                    if (sessionInstance == null)
                    {
                        sessionInstance = new ProcessManager();
                        HttpContext.Current.Session[SESSION_KEY] = sessionInstance;
                    }
                    _processManagerInstance = sessionInstance;

                }
            }

            return _processManagerInstance;
        }

        /// <summary>
        /// Gets the dictionary of processes from the session. If the session does on contain a dictionary to store processes, it is created.
        /// </summary>
        /// <returns></returns>
        public IDictionary<Guid, IProcess> GetProcesses()
        {
            if (_processes == null)
            {
                lock (_lock)
                {
                    var sessionInstance = HttpContext.Current.Session[PROCESSES_SESSION_KEY] as IDictionary<Guid, IProcess>;
                    if (sessionInstance == null)
                    {
                        sessionInstance = new Dictionary<Guid, IProcess>();
                        HttpContext.Current.Session[PROCESSES_SESSION_KEY] = sessionInstance;
                    }
                    _processes = sessionInstance;
                }
            }

            return _processes;
        }
    }
}
