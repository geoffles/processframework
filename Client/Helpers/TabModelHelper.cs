using System;
using System.Collections.Generic;

using ProcessFramework.Client.Common.Process;
using ProcessFramework.Client.Common.Validation;
using ProcessFramework.Client.Models;

namespace ProcessFramework.Client.Helpers
{
    /// <summary>
    /// The responsibility of this class is to provide helper methods related to the <see cref="TabModel"/> class.
    /// <remarks>
    /// <para>
    /// Created By  : Another Dev (anon for privacy)<br/>
    /// Date        : 2014-01-16<br/>
    /// </para>
    /// <para>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// </para>
    /// </remarks>
    /// </summary>
    internal static class TabModelHelper
    {
        /// <summary>
        /// Builds a tab model.
        /// </summary>
        /// <param name="viewModel">The view model to place on the tab model.</param>
        /// <param name="validationFailed">Indicator of whether this model is being built in response to a validation failure.</param>
        /// <returns>The tab model built.</returns>
        internal static TabModel BuildTabModel(object viewModel, bool validationFailed = false)
        {
            IProcess process = ProcessManager.Instance.WebActionProcess;

            TabModel tabModel = new TabModel
                {
                    Model = viewModel,
                    TabId = process.TabId,
                    ObjectiveIssues = ValidationManager.Instance.GetErrors(),
                    ProcessStates = ProcessManager.Instance.GetCurrentProcessStateModel(),
                    TabDescription = ProcessManager.Instance.GetProcessDescription(),
                    ViewIdentifier = ProcessManager.Instance.GetCurrentView(process),
                    ValidationFailure = validationFailed
                };

            ProcessCompletedResult result = process.CurrentState as ProcessCompletedResult;
            if (result != null)
            {
                var state = result;
                tabModel.CloseTab = true;
                if (state.WrapAroundProcess)
                {
                    tabModel.ReInitialise = true;
                    tabModel.Transitions = new List<ProcessMetaModel>
                        {
                            ProcessManager.Instance.GetProcessIntialiser(process.GetType())
                        };
                }
            }
            return tabModel;
        }
    }
}
