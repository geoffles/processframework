using System;
using System.Collections.Generic;
using ProcessFramework.Client.Common.Process;
using ProcessFramework.Client.Common.Validation;

namespace ProcessFramework.Client.Models
{
    /// <summary>
    /// The responsibility of this class is to provide the current process state to the client.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-09-03<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public sealed class TabModel
    {
        /// <summary>
        /// Gets or sets the process states.
        /// </summary>
        public IList<ProcessStateModel> ProcessStates { get; set; }

        /// <summary>
        /// Gets or sets the objective issues.
        /// </summary>
        public IList<ProcessValidationError> ObjectiveIssues { get; set; }

        /// <summary>
        /// Gets or sets the current data model
        /// </summary>
        public object Model { get; set; }

        /// <summary>
        /// Gets or sets the current tabId
        /// </summary>
        public Guid TabId { get; set; }

        /// <summary>
        /// Gets or sets a flag indicating to the client if the tab should be closed.
        /// </summary>
        public bool CloseTab { get; set; }

        /// <summary>
        /// Gets or sets the tab description from the process.
        /// </summary>
        public string TabDescription { get; set; }
        
        /// <summary>
        /// Indicates that the tab should be loaded from the initialise step again.
        /// </summary>
        public bool ReInitialise { get; set; }

        /// <summary>
        /// Gets or sets a list of available transitions from the new state. Not currently in use except for process wrap around
        /// </summary>
        public IList<ProcessMetaModel> Transitions { get; set; }

        public ResourceIdentifier ViewIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the validation failure flag.
        /// </summary>
        public bool ValidationFailure { get; set; }
    }
}
