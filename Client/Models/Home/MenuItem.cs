using System;
using System.Collections.Generic;
using System.Text;
using ProcessFramework.Client.Common.Process;

namespace ProcessFramework.Client.Models.Home
{
    /// <summary>
    /// The responsibility of this class is to provide a view model for LHS Menu items
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-09-03<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public sealed class MenuItem
    {
        /// <summary>
        /// The text displayed on this item
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// The metadata for generating a link to the target process for the item
        /// </summary>
        public ProcessMetaModel ProcessMetaModel { get; set; }
    }
}
