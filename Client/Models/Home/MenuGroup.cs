using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProcessFramework.Client.Common.Process;
using ProcessFramework.Client.Common.Process.Menu;

namespace ProcessFramework.Client.Models.Home
{
    /// <summary>
    /// The responsibility of this class is to provide a view model for LHS menu item groups.
    /// <remarks>
    /// Created By  : Geoffrey Lydall<br/>
    /// Date        : 2013-09-03<br/>
    /// <para/>
    /// Modified By : <br/>
    /// Date        : <br/>
    /// Details     : <br/>
    /// <para/>
    /// </remarks>
    /// </summary>
    public sealed class MenuGroup
    {
        /// <summary>
        /// The text to be displayed on the menu grouping.
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// The items in this grouping
        /// </summary>
        public IList<MenuItem> GroupItems { get; set; }
    }
}
